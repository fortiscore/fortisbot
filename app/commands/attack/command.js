const BasicCommand = use('classes/BasicCommand');
const strings      = require('./strings.json').strings;

/**
 * @alias Command.attack
 * @extends _.BasicCommand
 */
class Command extends BasicCommand {
  /**
   * runs the plugin code when called
   */
  init() {
    Promise.all([
      this._auriga.loadService('discord')
    ]).then((requirements) => {
      this.discord = requirements[0];
      this.register();
    });
  }

  /**
   * runs the command code when called
   * @param {string} message the message received from the service
   * @param {object} bundle the bundled meta data provided by the service
   * @param {string} service the service that called the action
   * @return {promise} resolves with response when finished, rejects on errors
   */
  action(message, bundle, service) {
    return new Promise((resolve, reject) => {
      if (service._name !== 'discord') {
        reject();
      }

      // don't support direct messages
      let channel = this.discord.bot.channels.cache.get(bundle.channelID);
      if (!channel || !channel.guild || !channel.guild.id) {
        resolve('There is nobody to attack...');
      }

      const serverID = channel.guild.id;
      const from = this.discord.getDisplayNameByID(serverID, bundle.userID);
      let attack = strings[Math.floor(Math.random()*strings.length)]
                   .replace(/\%u/g, '`' + from + '`')
                   .replace(/\%w/g, '`' + message + '`');

      const regexp = /(<\@!?[a-zA-Z0-9]+>)/g;

      let m;
      let matches = [];
      do {
        m = regexp.exec(attack);
        if (m) {
            matches.push(m[1]);
        }
      } while(m);
      let userid;
      for (let i=0; i < matches.length; i++) {
        userid = matches[i].substring(2, matches[i].length - 1);
        if (userid.startsWith('!')) {
          userid = userid.substring(1);
        }
        let name = this.discord.getDisplayNameByID(serverID, userid);
        attack = attack.replace(new RegExp(matches[i], 'g'), name);
      }

      let damage = Math.floor(Math.random() * 9999);
      const rand   = Math.floor(Math.random() * 100) + 1;
      if (rand < 15) {
        damage = Math.floor(damage/2) + ' (glancing)';
      } else if (rand > 90) {
        damage = Math.floor(damage*2) + ' (CRITICAL)';
      }

      resolve(`:zap: ${attack} for *\`${damage} damage.\`*`);
    });
  }
};

module.exports = Command;
