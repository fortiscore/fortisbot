const BasicCommand = use('classes/BasicCommand');

/**
 * @alias Command.choose
 * @extends _.BasicCommand
 */
class Command extends BasicCommand {

  /**
   * runs the command code when called
   * @param {string} message the message received from the service
   * @param {object} bundle the bundled meta data provided by the service
   * @param {string} service the service that called the action
   * @return {promise} resolves with response when finished, rejects on errors
   */
  action(message, bundle, service) {
    return new Promise((resolve, reject) => {
      if (message === '') {
        resolve('You didn\'t give me anything to choose from!');
      } else {
        let choices = message.split(',');
        const choice = choices[Math.floor(Math.random()*choices.length)];

        resolve(choice.trim());
      }
    });
  }
};

module.exports = Command;
