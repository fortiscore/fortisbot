const BasicCommand = use('classes/BasicCommand');

/**
 * @alias Command.dice
 * @extends _.BasicCommand
 */
class Command extends BasicCommand {
  /**
   * runs the command code when called
   * @param {string} message the message received from the service
   * @param {object} bundle the bundled meta data provided by the service
   * @return {promise} resolves with response when finished, rejects on errors
   */
  action(message, bundle) {
    return new Promise((resolve, reject) => {

      if (message === '') {
          // regular dice
          const dice = Math.floor(Math.random() * 6) + 1;
          resolve(`rolled a ${dice}`);
      } else {
          // look for special format
          const regexp = /^([0-9]+)d([0-9]+)([\+|\-][0-9]+)?/g;
          const match  = regexp.exec(message);

          if (match && match.length > 0) {
              let dice  = parseInt(match[1]);
              let sides = parseInt(match[2]);
              let mod   = match[3];
              let msg;

              if (dice > 100)  {
                dice  = 100;
              } else if (dice  < 1) {
                dice  = 1;
              }
              if (sides > 100) {
                sides = 100;
              } else if (sides < 1) {
                sides = 1;
              }

              let roll;
              let rolls = [];
              let i;
              for (i = 1; i <= dice; i++) {
                  roll = Math.floor(Math.random() * sides) + 1;
                  rolls.push(roll);
              }
              let total = 0;
              rolls.forEach(function(entry) {
                  total += entry;
              });

              const rollMsg = dice + 'd' + sides;

              const min         = this.getLowest(rolls);
              const max         = this.getHighest(rolls);
              const avg         = this.getAverage(rolls);
              const rollCompact = `min: ${min}, max: ${max}, avg: ${avg}`;

              if (mod) {
                  const modnum = mod.replace('+', '');
                  if (modnum.startsWith('-')) {
                      mod = modnum;
                  }
                  total = total + parseInt(modnum);
                  msg = `rolled ${total} using ${rollMsg}${mod} (${rollCompact})`;
              } else {
                  msg = `rolled ${total} using ${rollMsg} (${rollCompact})`;
              }

              resolve(msg);
          }
      }

    });
  }

  /**
   * returns an average number from an array
   * @param {array} arr an array of numbers
   * @return {number} the average of the array
   */
  getAverage(arr) {
    const sum = arr.reduce((a, b) => a + b);
    const avg = sum / arr.length;
    return Math.floor(avg);
  }

  /**
   * returns the lowest number in an array
   * @param {array} arr an array of numbers
   * @return {number} the lowest number in the array
   */
  getLowest(arr) {
    return Math.min(...arr);
  }

  /**
   * returns the highest number in an array
   * @param {array} arr an array of numbers
   * @return {number} the highest number in the array
   */
  getHighest(arr) {
    return Math.max(...arr);
  }
};

module.exports = Command;
