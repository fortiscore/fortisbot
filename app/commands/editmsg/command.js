const BasicCommand = use('classes/BasicCommand');

/**
 * @alias Command.editmessage
 * @extends _.BasicCommand
 */
class Command extends BasicCommand {
  /**
   * code to run on load
   */
  init() {
    this._auriga.loadService('discord').then((discord) => {
      this.discord = discord;
      this.bot     = discord.bot;
      this.register();
    });
  }

  /**
   * runs the command code when called
   * @param {string} message the message received from the service
   * @param {object} bundle the bundled meta data provided by the service
   * @param {string} service the service that called the action
   * @return {promise} resolves with response when finished, rejects on errors
   */
  action(message, bundle, service) {
    if (service._name !== 'discord') {
      return Promise.reject();
    }

    return new Promise((resolve, reject) => {
      const messageParts = this._auriga.extractCommandFromString(message);
      let url = messageParts.cmd;
      let msg = messageParts.str;

      let regexp = /discordapp.com\/channels\/([0-9]+)\/([0-9]+)\/([0-9]+)/;
      let matches = url.match(regexp);
      if (!matches) {
        resolve('Incorrectly formatted.');
      }

      const channelID  = matches[2];
      const messageID  = matches[3];

      resolve();
      this.bot.channels.cache.get(channelID).messages.fetch(messageID)
      .then(message => {
          message.edit(msg);
          resolve('Message editied');
      })
      .catch(resolve('Couldn\'t edit that message'));
    });
  }
};

module.exports = Command;
