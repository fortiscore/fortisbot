const BasicCommand = use('classes/BasicCommand');
const util         = require('util');

/**
 * @alias Command.eval
 * @extends _.BasicCommand
 */
class Command extends BasicCommand {
  /**
   * runs the command code when called
   * @param {string} message the message received from the service
   * @param {object} bundle the bundled meta data provided by the service
   * @return {promise} resolves with response when finished, rejects on errors
   */
  action(message, bundle) {
    return new Promise((resolve, reject) => {
      if (
        (message && message.toLowerCase().includes('settings')) ||
        (message && message.toLowerCase().includes('config'))
      ) {
        resolve('```js\nFORBIDDEN: Looking for `settings` is not allowed.\n```');
      } else {
        let output = false;

        try {
          let username      = bundle.source.author.username;
          let discriminator = bundle.source.author.discriminator;
          let user          = `${username}#${discriminator}`;
          this.info(`'${user}' ran command '${message}'`);
          output = eval(message);
          if (typeof output === 'object') {
            output = util.inspect(output, {depth: 0});
            let maxLength = 1500;
            if (output.length >= maxLength) {
              output = output.substring(0, maxLength) + ' \n\n...[truncated]';
            }
          }
        } catch (err) {
          output = err;
        }

        if (!output) {
          // no output
          resolve('```Completed without errors```');
        } else if (
          output.toLowerCase().includes('oauth') ||
          output.toLowerCase().includes('pass') ||
          output.toLowerCase().includes('token')
        ) {
          // output contains sensitive data
          resolve('```js\nFORBIDDEN: Results contain sensitive information\n```');
        } else {
          // output has text results
          resolve('```js\n' + output + '\n```');
        }
      }
    });
  }
};

module.exports = Command;
