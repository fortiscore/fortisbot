const BasicCommand = use('classes/BasicCommand');

/**
 * @alias Command.game
 * @extends _.BasicCommand
 */
class Command extends BasicCommand {
  /**
   * runs the plugin code when called
   */
  init() {
    Promise.all([
      this._auriga.loadPlugin('giantBombApi')
    ]).then((requirements) => {
      this.giantBomb = requirements[0];
      this.register();
    });

    this._auriga.loadService('webLocalServer').then((web) => {
      this.webIcons = web.registerStatic(`${__dirname}/icons`, this);
    });
  }

  /**
   * runs the command code when called
   * @param {string} message the message received from the service
   * @param {object} bundle the bundled meta data provided by the service
   * @param {string} service the service that called the action
   * @return {promise} resolves with response when finished, rejects on errors
   */
  action(message, bundle, service) {
    return new Promise((resolve, reject) => {
      // only run on discord
      if (service._name !== 'discord') {
        reject();
      }

      // determine user provided parameters {query, listMode, selected}
      let params = this.parseMessageParams(message);
      if (params.query === '') {
        resolve('You didn\'t search for anything!');
      } else {
        // search giantbomb games
        this.giantBomb.searchGames(params.query).then((results) => {
          if (params.listMode) {
            resolve(this.buildListResponse(bundle, results));
          } else {
            resolve(this.buildEntryResponse(bundle, results, params.selected));
          }
        }).catch((err) => {
          this.warn(err);
          resolve(err);
        });
      }
    });
  }

  /**
   * builds a list response
   * @param {object} bundle the service bundle provided to the command by aurigo
   * @param {array} results the array of results provided by giantbomb api
   * @return {object} an object response to resolve back to the service
   */
  buildListResponse(bundle, results) {
    let embedObj = this.getEmbedTemplate();

    let list        = '';
    let counter     = 0;
    const resultSet =  results.slice(0, 10);

    resultSet.forEach((entry) => {
      counter++;
      list += `\`${counter}\`. ${entry.name}\n`;
    });
    embedObj.title       = `Game Search Results`;
    embedObj.description = `Use the \`--1\`, \`--2\` (etc) flag ` +
    `at the end of your search query to select a specific entry ` +
    `from the list.\n\n${list}\n _ _`;

    return {
      to: bundle.channelID,
      embed: embedObj
    };
  }

  /**
   * builds an entry response
   * @param {object} bundle the service bundle provided to the command by aurigo
   * @param {array} results the array of results provided by giantbomb api
   * @param {number} selected the selected entry index
   * @return {object} an object response to resolve back to the service
   */
  buildEntryResponse(bundle, results, selected=0) {
    // get the active index
    let activeIndex = 0;
    if (selected) {
      activeIndex = results[selected] ? selected : 0;
    } else {
      activeIndex = 0;
    }

    // prepare the embed
    let embedObj   = this.getEmbedTemplate();
    const entry    = results[activeIndex];
    embedObj.url   = entry.site_detail_url;
    embedObj.title = entry.name;

    // fill the embed with optional data
    if (entry.image && entry.image.thumb_url) {
      embedObj.thumbnail = {
        url: entry.image.thumb_url
      };
    }

    if (entry.original_release_date) {
      embedObj.fields.push({
        name:   'Release Date',
        value:  entry.original_release_date,
        inline: true
      });
    }

    if (entry.original_game_rating) {
      const ratingNames = entry.original_game_rating.map(x => x.name);
      embedObj.fields.push({
        name:   'Ratings',
        value:  ratingNames.join(', '),
        inline: true
      });
    }

    if (entry.deck) {
      const maxLength = 1014;
      let synopsis    = entry.deck;
      if (synopsis.length > maxLength) {
        synopsis = `${synopsis.slice(0, maxLength - 6)} [...]`;
      }
      embedObj.fields.push({
        name:   'Synopsis',
        value:  synopsis,
        inline: false
      });
    }

    if (entry.platforms) {
      const platformNames = entry.platforms.map((x) => x.name);
      embedObj.fields.push({
        name:   'Platforms',
        value:  platformNames.join(', '),
        inline: false
      });
    }

    return {
      to: bundle.channelID,
      embed: embedObj
    };
  }

  /**
   * returns the default embed object template
   * @return {object} the embed object template
   */
  getEmbedTemplate() {
    let template = {
      thumbnail: {
        url: ''
      },
      color: 0xA21616,
      fields: [],
      footer: {
        text: 'Details provided by GiantBomb.com',
      }
    };

    if (this.webIcons) {
      template.footer.icon_url = `${this.webIcons}/logo.png?v=1`;
    }

    return template;
  }

  /**
   * parses raw user input as message with optional listing parameters
   * @param {string} input the raw user input query
   * @return {object} an object containing relevant parameters
   */
  parseMessageParams(input) {
    let params = {
      query: input,
      listMode: false,
      selected: false
    };

    const selectData  = input.match(/\-\-(\d+)$/);

    if (input.endsWith('--list')) {
      params.listMode = true;
      params.query  = input.replace(/\-\-list/g, '');
    } else if (selectData) {
      params.selected = selectData[0].replace(/\-/g, '') - 1;
      params.query  = input.replace(selectData[0], '');
    }
    params.query = params.query.trim().toLowerCase();

    return params;
  }
};

module.exports = Command;
