const BasicCommand = use('classes/BasicCommand');

/**
 * @alias Command.group
 * @extends _.BasicCommand
 */
class Command extends BasicCommand {
  /**
   * runs the command code when called
   * @param {string} message the message received from the service
   * @param {object} bundle the bundled meta data provided by the service
   * @param {string} service the service that called the action
   * @return {promise} resolves with response when finished, rejects on errors
   */
  action(message, bundle, service) {
    if (service._name !== 'discord') {
      return Promise.reject();
    }

    return new Promise((resolve, reject) => {
      this._auriga.loadService('discord')
        .then((discord) => {
          // don't support direct messages
          let channel = discord.bot.channels.cache.get(bundle.channelID);
          if (!channel || !channel.guild || !channel.guild.id) {
            resolve('This must be run from within a valid server');
          }

          const serverID = (channel) ? channel.guild.id : null;
          if (serverID) {
            let channel = discord.bot.channels.cache.get(bundle.channelID);
            if (!channel || !channel.guild || !channel.guild.id) {
              resolve('This must be run from within a valid server');
            }

            const server = discord.bot.guilds.cache.get(serverID);
            const member = server.members.cache.get(bundle.userID);

            let parts = this._auriga.extractCommandFromString(message);
            switch(parts.cmd) {
              case 'join':
                const toJoin = server.roles.find((x) => {
                    return x.name.toLowerCase() === (`⚑ ${parts.str}`.toLowerCase());
                });

                if (toJoin) {
                  member.addRole(toJoin.id);
                  resolve(`You have joined \`${toJoin.name}\``);
                } else {
                  resolve(`group \`⚑ ${parts.str}\` does not exist.`);
                }

                break;
              case 'leave':
                const toLeave = server.roles.find((x) => {
                    return x.name.toLowerCase() === (`⚑ ${parts.str}`.toLowerCase());
                });

                if (toLeave) {
                  member.removeRole(toLeave.id);
                  resolve(`You have left \`${toLeave.name}\``);
                } else {
                  resolve(`group \`⚑ ${parts.str}\` does not exist.`);
                }

                break;
              default:
                resolve('the group command requires either a join or leave');
            }
          }
        })
        .catch((err) => {
          reject(err);
        });
    });
  }
};

module.exports = Command;
