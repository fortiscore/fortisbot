const BasicCommand = use('classes/BasicCommand');

/**
 * @alias Command.group
 * @extends _.BasicCommand
 */
class Command extends BasicCommand {
  /**
   * code to run on load
   */
  init() {
    this._auriga.loadService('discord').then((discord) => {
      this.discord = discord;
      this.bot     = discord.bot;
      this.register();
    });
  }
  /**
   * runs the command code when called
   * @param {string} message the message received from the service
   * @param {object} bundle the bundled meta data provided by the service
   * @param {string} service the service that called the action
   * @return {promise} resolves with response when finished, rejects on errors
   */
  action(message, bundle, service) {
    if (service._name !== 'discord') {
      return Promise.reject();
    }

    return new Promise((resolve, reject) => {
      let discord = this.discord;

      // don't support direct messages
      let channel = discord.bot.channels.cache.get(bundle.channelID);
      if (!channel || !channel.guild || !channel.guild.id) {
        resolve('This must be run from within a valid server');
      }

      const serverID = (channel) ? channel.guild.id : null;
      if (serverID) {
        let channel = discord.bot.channels.cache.get(bundle.channelID);
        if (!channel || !channel.guild || !channel.guild.id) {
          resolve('This must be run from within a valid server');
        }

        const server = discord.bot.guilds.cache.get(serverID);

        let parts = this._auriga.extractCommandFromString(message);
        switch(parts.cmd) {
          case 'add':
            const exists = server.roles.find((x) => {
              return x.name.toLowerCase() === (`⚑ ${parts.str}`.toLowerCase());
            });

            if (exists) {
              resolve('That group already exists');
            } else {
              server.createRole({
                name: `⚑ ${parts.str}`,
                mentionable: true
              }).then((role) => {
                let successMsg = `Users can now join with  \`group join ${parts.str.toLowerCase()}\`, and participate in the group specific chat room!`;
                let existingChannel = server.channels.find((channel) => {
                  return channel.name === `⚑ ${parts.str}`;
                });
                if (!existingChannel) {
                  this.createTextChannel(role, serverID)
                 .then(resolve(successMsg))
                 .catch(resolve);
                } else {
                  resolve(successMsg);
                }
              });
            }
            break;
          case 'remove':
            const toRemove = server.roles.find((x) => {
              return x.name.toLowerCase() === (`⚑ ${parts.str}`.toLowerCase());
            });

            if (toRemove) {
              server.roles.cache.get(toRemove.id).delete();
              let existingChannel = server.channels.find((channel) => {
                return channel.name === `⚑ ${parts.str}`.split(' ').join('-');
              });
              if (existingChannel) {
                existingChannel.delete();
              }
              resolve(`Removed group \`⚑ ${parts.str}\``);
            } else {
              resolve('That group does not exist');
            }
            break;
          default:
            resolve('the groupmod command requires either add or remove');
        }
      }
    });
  }

  /** creates a text channel based on the role provided
   * @param {object} role the role object provided from discordjs
   * @param {string} serverID the server id
   * @return {promise} resolve on successs, reject on failure
  */
  createTextChannel(role, serverID) {
    return new Promise((resolve, reject) => {
      // configure permission overwrites
      const overwrites = [];

      // get room category
      let catID = this._settings.serverRoomCategoryID[serverID];
      if (this._settings.serverRoomCategoryID && catID) {
        // create the text channel under the proper category
        const guild = this.bot.guilds.cache.get(serverID);
        const opts = {
          type: 'text',
          position: 0,
          lockPermissions:false
        };
        if (this.bot.channels.cache.get(catID)) {
          guild.createChannel(`${role.name}`, 'text')
          .then(channel => {
            if (catID) {
              // provision the channel permissions
              /*
              channel.overwritePermissions(serverID, {
                SEND_MESSAGES: false
              });
              channel.overwritePermissions(role.id, {
                SEND_MESSAGES: true
              });
              channel.setParent(catID);

              resolve(`completed`);
              */
            }
          });
        }

      } else {
        reject(`category doesn't exist`);
      }
    });
  }
};

module.exports = Command;
