const BasicCommand = use('classes/BasicCommand');

/**
 * @alias Command.help
 * @extends _.BasicCommand
 */
class Command extends BasicCommand {
  /**
   * runs the command code when called
   * @param {string} message the message received from the service
   * @param {object} bundle the bundled meta data provided by the service
   * @return {promise} resolves with response when finished, rejects on errors
   */
  action(message, bundle) {
    return new Promise((resolve, reject) => {

      resolve(`${this._settings.documentation_message}`);

    });
  }
};

module.exports = Command;
