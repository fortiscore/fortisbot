const BasicCommand = use('classes/BasicCommand');
const math         = require('mathjs');

/**
 * @alias Command.math
 * @extends _.BasicCommand
 */
class Command extends BasicCommand {
  /**
   * runs the command code when called
   * @param {string} message the message received from the service
   * @param {object} bundle the bundled meta data provided by the service
   * @return {promise} resolves with response when finished, rejects on errors
   */
  action(message, bundle) {
    return new Promise((resolve, reject) => {
      let output;
      try {
        output = math.eval(message);
      } catch(err) {
        output =  'I don\'t know how to evaluate that expression!';
      }
      if (isNaN(output)) {
        output = 'I don\'t know how to evaluate that expression!';
      }
      resolve(output);
    });
  }
};

module.exports = Command;
