const BasicCommand = use('classes/BasicCommand');

/**
 * @alias Command.quotemod
 * @extends _.BasicCommand
 */
class Command extends BasicCommand {
  /**
   * code to run on load
   */
  init() {
    this._auriga.loadCommand('quote').then((quoteCmd) => {
      this.storage = this._auriga.storage;
      this.qm      = quoteCmd;
      this.register();
    });
  }

  /**
   * runs the command code when called
   * @param {string} message the message received from the service
   * @param {object} bundle the bundled meta data provided by the service
   * @param {object} service the service that called the action
   * @return {promise} resolves with response when finished, rejects on errors
   */
  action(message, bundle, service) {
    return new Promise((resolve, reject) => {
      let messageParts = this._auriga.extractCommandFromString(message);
      switch(messageParts.cmd.toLowerCase()) {
        case 'find':
          this.findQuotes(messageParts.str).then((quotes) => {
            const count = quotes.length;
            let cut     = 0;
            const max   = 10;
            if (quotes.length > 10) {
              cut = quotes.length - 10;
              quotes = quotes.slice(0, 10);
            }
            if (service._name === 'discord') {
              quotes = quotes.map(x => `\`${x._id}\`: ${x.quote}\n`);
              let message = (`Showing ${quotes.length} quote(s):\n\n${quotes.join('')}`);
              if (cut > 0) {
                message+= `\n${cut} more matching quotes found. Try more specific searches.`;
              }
              resolve(message);
            } else {
              resolve(`found ${count} quote(s). Use the command in discord for more management`);
            }
          }).catch(resolve);
          break;
        case 'findremove':
          let self = this;
          this.findQuotes(messageParts.str).then((quotes) => {
            quotes = quotes.map(x => x._id);
            let count = quotes.length;
            quotes.forEach((quoteID) => {
              self.deleteQuote(quoteID)
              .catch(self.warn);
            });
            resolve(`Found and removed ${count} quote(s).`);
          }).catch(resolve);
          break;
        case 'add':
          this.addQuote(messageParts.str).then((quote) => {
            resolve(`added quote!`);
          }).catch(resolve);
          break;
        case 'remove':
          this.deleteQuote(messageParts.str).then(() => {
            resolve(`deleted quote`);
          }).catch(resolve);
          break;
        case 'update':
          let updateParts = this._auriga.extractCommandFromString(messageParts.str);
          this.updateQuote(updateParts.cmd, updateParts.str).then(() => {
            resolve(`updated quote`);
          }).catch(resolve);
          break;
        default:
          resolve('no valid action provided. Actions allowed: `find` `add` `remove` `update`');
          break;
      }
    });
  }

  /**
   * adds a quote to the database
   * @param {string} message the message to add to the database
   * @return {promise} resolves on success, rejects on errors
   */
  addQuote(message) {
    return new Promise((resolve, reject) => {
      if (message.length < 1) {
        reject('No quote provided');
      }

      this.storage.insert({type:'fortis-quote', quote:message}, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }

  /**
   * finds quotes that match the search query
   * @param {string} str the text to search for
   * @return {promise} resolves with quotes on success, rejects on errors
   */
  findQuotes(str) {
    return new Promise((resolve, reject) => {
      if (str.length < 3) {
        reject('please use at least 3 characters in your search query');
      }
      const reg = new RegExp(str, 'gi');
      this.storage.find({type:'fortis-quote', quote:reg}, (err, quotes) => {
        if (err || quotes.length === 0) {
          reject('couldn\'t find any quotes');
        } else {
          resolve(quotes);
        }
      });
    });
  }

  /**
   * deletes quotes that have the provided ID
   * @param {string} id the quote _id as stored in the database
   * @return {promise} resolves on success, rejects with message on errors
   */
  deleteQuote(id) {
    return new Promise((resolve, reject) => {
      id = id.trim();
      this.storage.remove({type: 'fortis-quote', _id: id}, {},
      (err, numRemoved) => {
        if (err || numRemoved < 1) {
          reject('couldn\'t remove the chosen quote');
        } else {
          resolve();
        }
      });
    });
  }

  /**
   * updates a saved quote
   * @param {string} oldID the id of the quote to update
   * @param {string} newQuote the new quote text to use
   * @return {promise} resolves on success, rejects on errors
   */
  updateQuote(oldID, newQuote) {
    return new Promise((resolve, reject) => {
      this.storage.update(
        {type:'fortis-quote', _id:oldID},
        {type:'fortis-quote', quote:newQuote},
        {upsert:true},
        (err, numReplaced, upsert) => {
          if (err) {
            reject(err);
          } else {
            resolve();
          }
        }
      );
    });
  }

};

module.exports = Command;
