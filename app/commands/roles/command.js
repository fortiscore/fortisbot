const BasicCommand = use('classes/BasicCommand');

/**
 * @alias Command.roles
 * @extends _.BasicCommand
 */
class Command extends BasicCommand {
  /**
   * runs the command code when called
   * @param {string} message the message received from the service
   * @param {object} bundle the bundled meta data provided by the service
   * @param {string} service the service that called the action
   * @return {promise} resolves with response when finished, rejects on errors
   */
  action(message, bundle, service) {
    if (service._name !== 'discord') {
      return Promise.reject();
    }

    return new Promise((resolve, reject) => {
      this._auriga.loadService('discord')
        .then((discord) => {
          // don't support direct messages
          let channel = discord.bot.channels.cache.get(bundle.channelID);
          if (!channel || !channel.guild || !channel.guild.id) {
            resolve('This must be run from within a valid server');
          }

          const serverID = (channel) ? channel.guild.id : null;
          if (serverID) {
            const server = discord.bot.guilds.cache.get(serverID);
            const roles = server.roles.cache.map((x) => {
              let id   = x.id;
              let name = (x.name === '@everyone')
                       ? 'everyone'
                       : x.name;
              return `[${name}](${id})`;
            });
            resolve(`\n\`\`\`md\n#SERVER ROLES\n${roles.sort().join('\n')}\n\`\`\``);
          } else {
            reject(this.error(`${serverID} is not a valid server.`));
          }
        })
        .catch((err) => {
          this.warn(err);
          reject(err);
        });
    });
  }
};

module.exports = Command;
