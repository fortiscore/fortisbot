const BasicCommand = use('classes/BasicCommand');

/**
 * @alias Command.room
 * @extends _.BasicCommand
 */
class Command extends BasicCommand {
  /**
   * code to run on load
   */
  init() {
    this._auriga.loadService('discord').then((discord) => {
      this.discord = discord;
      this.bot     = discord.bot;
      this.storage = this._auriga.storage;
      this.register();
      setTimeout(() => {
        this._auriga.registerTask('room-purge', 1, this.purgeOldRooms.bind(this));
      }, 5000);
    });
  }

  /**
   * runs the command code when called
   * @param {string} message the message received from the service
   * @param {object} bundle the bundled meta data provided by the service
   * @param {string} service the service that called the action
   * @return {promise} resolves with response when finished, rejects on errors
   */
  action(message, bundle, service) {
    if (service._name !== 'discord') {
      return Promise.reject();
    }

    const userID    = bundle.userID;
    const channelID = bundle.channelID;
    const channel   = this.bot.channels.cache.get(channelID);

    if (!channel || channel.type === 'dm') {
      return Promise.resolve('You must be in a valid server to run this command.');
    }

    return new Promise((resolve, reject) => {
      const prefix       = this._auriga.config.prefix;
      const serverID     = channel.guild.id;
      const messageParts = this._auriga.extractCommandFromString(message);
      const displayName  = this.discord.getDisplayNameByID(serverID, userID);

      let payload;
      switch(messageParts.cmd.toLowerCase()) {
        case 'x':
          // remove the command message
          // bundle.source.delete();

          // remove a room
          payload = {
            userID: userID,
            serverID: serverID
          };
          this.getRoom(payload)
            .then(this.removeRoom.bind(this))
            .then(resolve('Room deleted.'))
            .catch(resolve);
          break;

        case 'invite':
          // remove the command message
          // bundle.source.delete();

          // invite people to private room
          payload = {
            userID: userID,
            serverID: serverID
          };
          this.getRoom(payload)
            .then((payload) => {

              const ownerID = payload.userID;
              const users   = bundle.source.mentions.users.map(x => x.id);
              const roles   = bundle.source.mentions.roles.map(x => x.id);
              const invites = users.concat(roles);

              payload.roomID = payload.channel.roomID;
              if (
                payload &&
                payload.channel &&
                payload.channel.privacy === 'public'
              ) {
                let respondWith = `Your room is a public room which doesn't need invitations!`;
                // this.respondPrivately(payload, respondWith);
                resolve(respondWith);
              } else if (users.length === 0 && roles.length === 0) {
                let respondWith = `You didn\'t invite anyone! use \`${prefix}room invite @user @user\``;
                // this.respondPrivately(payload, respondWith);
                resolve(respondWith);
              } else {
                const processInvites = () => {
                  payload.userID = invites.shift();
                  payload.isInvites = true;
                  this.addUserToChannelPermissions(payload)
                  .then(() => {
                    if (invites.length === 0) {
                      payload.userID = ownerID;
                      let respondWith = 'All Invites Sent';
                      // this.respondPrivately(payload, respondWith);
                      resolve(respondWith);
                    } else {
                      processInvites();
                    }
                  }).catch(this.warn);
                };
                processInvites();
              }
            })
            .catch(resolve);
          break;

        case 'private':
          // remove the command message
          // bundle.source.delete();

          // create a private room
          payload = {
            name: (messageParts.str === '')
                  ? `${displayName}'s room`
                  : messageParts.str,
            userID: userID,
            serverID: serverID,
            privacy: 'private'
          };
          payload.isPrivateRoom = true;
          this.ensureUserRoomDoesntExist(payload)
            .then(this.createRoom.bind(this))
            .then((payload) => {
              // send private dm to user
              let respondWith = `Private voice channel \`${payload.name}\` created! Use \`${prefix}room invite @user @user\` to invite people, roles, or otherwise!  This room will automatically delete when empty.`;
              // this.respondPrivately(payload, respondWith);
              resolve(respondWith);
            })
            .catch(resolve);
          break;

        default:
          // create a public room
          payload = {
            name: (message === '')
                  ? `${displayName}'s room`
                  : message,
            userID: bundle.userID,
            serverID: bundle.serverID,
            privacy: 'public'
          };

          this.ensureUserRoomDoesntExist(payload)
            .then(this.createRoom.bind(this))
            .then((payload) => {
              resolve(`Public voice channel \`${payload.name}\` created!`);
            })
            .catch(resolve);
          break;
      }
    });
  }

  /**
   * sends a DM to the user that called the command
   * @param {object} payload an object containing parameters
   * @param {string} payload.userID the user's id
   * @param {string} respondWith the message to send
   */
  respondPrivately(payload, respondWith) {
    this.discord.respond({
      channelID: payload.userID,
      response: {
        to: payload.userID,
        message: respondWith
      }
    });
  }

  /**
   * creates a public voice chat room and adds the user to the overwrites
   * @param {object} payload an object containing parameters
   * @param {string} payload.name the name of the room
   * @param {string} payload.userID the user's id
   * @param {string} payload.serverID the id of the target server
   * @return {promise} resolves on success, rejects on errors
   */
  createRoom(payload) {
    return new Promise((resolve, reject) => {
      this.getRoomCategory(payload).then((catID) => {
        const guild = this.bot.guilds.cache.get(payload.serverID);
        const opts = {
          type: 'voice',
          position: 0,
          lockPermissions:false,
          permissionOverwrites: [
            {
              id: payload.userID,
              allow: [
                'VIEW_CHANNEL',
                'CONNECT',
                'SPEAK',
                'MUTE_MEMBERS',
                'DEAFEN_MEMBERS',
                'PRIORITY_SPEAKER',
                'USE_VAD'
              ]
            }
          ]
        };

        if (payload.isPrivateRoom) {
          opts.permissionOverwrites.push({
            id: payload.serverID,
            deny: [
              'CONNECT',
              'SPEAK',
              'MUTE_MEMBERS',
              'DEAFEN_MEMBERS',
              'MOVE_MEMBERS',
              'USE_VAD',
              'CREATE_INSTANT_INVITE',
              'MANAGE_ROLES',
              'MANAGE_CHANNELS'
            ]
          });
        }

        if (this.bot.channels.cache.get(catID)) {
          opts.parent = catID;
        }
        guild.channels.create(`${payload.name}`, opts)
        .then(channel => {
          channel.edit({position:0});
          this.storage.insert({
            type: 'user-room',
            privacy: payload.privacy,
            userID: payload.userID,
            serverID: payload.serverID,
            roomID: channel.id,
            created: Date.now()
          }, (err, resp) => {
            if (err) {
              reject(err);
            } else {
              payload.roomID = channel.id;
              resolve(payload);
            }
          });
        });
      });
    });
  }

  /**
   * removes a voice room from the server
   * @param {object} payload an object containing parameters
   * @param {string} payload.userID the user's id
   * @param {string} payload.serverID the id of the target server
   * @return {promise} resolves on success, rejects on errors
   */
  removeRoom(payload) {
    return new Promise((resolve, reject) => {
      this.storage.remove({
        type: 'user-room',
        userID: payload.userID,
        serverID: payload.serverID
      }, (err, numRemoved) => {
        if (err) {
          reject(err);
        } else {
          const channel = this.bot.channels.cache.get(payload.channel.roomID);
          if (channel) {
            channel.delete();
          }
          resolve('room successfully deleted');
        }
      });
    });
  }

  /**
   * gets an assigned room category for the server from config.yaml
   * @param {object} payload an object containing parameters
   * @param {string} payload.serverID the id of the target server
   * @return {promise} resolves on success, rejects on errors
   */
  getRoomCategory(payload) {
    return new Promise((resolve, reject) => {
      if (
        this._settings.serverRoomCategoryID &&
        this._settings.serverRoomCategoryID[payload.serverID]
      ) {
        resolve(this._settings.serverRoomCategoryID[payload.serverID]);
      } else {
        resolve(false);
      }
    });
  }

  /**
   * retrieves a user's room
   * @param {object} payload an object containing parameters
   * @param {string} payload.userID the user's id
   * @param {string} payload.serverID the id of the target server
   * @return {promise} resolves on success, rejects on errors
   */
  getRoom(payload) {
    return new Promise((resolve, reject) => {
      this.storage.findOne(
        {type: 'user-room', userID: payload.userID, serverID: payload.serverID},
        (err, room) => {
          if (err) {
            reject(err);
          } else if (room) {
            payload.channel = room;
            resolve(payload);
          } else {
            reject('You do not currently have any rooms open on this server');
          }
        }
      );
    });
  }

  /**
   * fetches a user's room and ensures it doesn't exist
   * @param {object} payload an object containing parameters
   * @param {string} payload.name the name of the room
   * @param {string} payload.userID the user's id
   * @param {string} payload.serverID the id of the target server
   * @return {promise} resolves with registered room if it exists
   */
  ensureUserRoomDoesntExist(payload) {
    return new Promise((resolve, reject) => {
      this.storage.findOne(
        {type: 'user-room', userID: payload.userID, serverID: payload.serverID},
        (err, room) => {
          if (room) {
            this.getRoom(payload)
            .then(this.removeRoom.bind(this))
            .then(resolve(payload));
          } else {
            resolve(payload);
          }
        }
      );
    });
  }

  /**
   * adds a user to channel permissions
   * @param {object} payload an object containing parameters
   * @param {string} payload.userID the user's id
   * @param {string} payload.roomID the channel id
   * @param {boolean} payload.isInvites true if invited party and not room owner
   * @return {promise} resolves on success, rejects on errors
  */
  addUserToChannelPermissions(payload) {
    return new Promise((resolve, reject) => {
      const channel = this.bot.channels.cache.get(payload.roomID);
      channel.updateOverwrite(
        payload.userID,
        {
          'VIEW_CHANNEL'    : true,
          'CONNECT'         : true,
          'SPEAK'           : true,
          'MUTE_MEMBERS'    : !payload.isInvites,
          'DEAFEN_MEMBERS'  : !payload.isInvites,
          'PRIORITY_SPEAKER': !payload.isInvites,
          'USE_VAD'         : true
        },
        'Bot Provisioning'
      )
      .then(resolve(payload))
      .catch((err) => {
        this.warn(err);
        reject(err);
      });
    });
  }

  /**
   * @todo
   * purges old rooms
   */
  purgeOldRooms() {
    const minUsers   = 1;
    const checkAfter = 1;
    const now        = Date.now();
    this.storage.find({type: 'user-room'}, (err, rooms) => {
      rooms.forEach((room) => {
        const channel = this.bot.channels.cache.get(room.roomID);
        if (channel && channel.members) {
          const memberCount = channel.members.map(x => x.id).length;
          const age = (now - parseInt(room.created)) / 1000 / 60;
          if (memberCount < minUsers && age >= checkAfter) {
            // delete the channel
            channel.delete().then(() => {
              // delete the database entry
              this.storage.remove({
                type:'user-room',
                roomID: room.roomID
              }, {}, (err, numRemoved) => {
              });
            });
          }
        }
      });
    });
  }
};

module.exports = Command;
