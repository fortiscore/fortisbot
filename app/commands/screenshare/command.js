const BasicCommand = use('classes/BasicCommand');

/**
 * @alias Command.screenshare
 * @extends _.BasicCommand
 */
class Command extends BasicCommand {
  /**
   * code to run on load
   */
  init() {
    this._auriga.loadService('discord').then((discord) => {
      this.bot     = discord.bot;
      this.register();
    });
  }

  /**
   * runs the command code when called
   * @param {string} message the message received from the service
   * @param {object} bundle the bundled meta data provided by the service
   * @param {string} service the service that called the action
   * @return {promise} resolves with response when finished, rejects on errors
   */
  action(message, bundle, service) {
    if (service._name !== 'discord') {
      return Promise.reject();
    }

    const channel  = this.bot.channels.cache.get(bundle.channelID);
    if (!channel || channel.type === 'dm') {
      return Promise.resolve('You must be in a valid server to run this command.');
    }

    return new Promise((resolve, reject) => {
      const serverID     = channel.guild.id;
      const user         = channel.guild.members.cache.get(bundle.userID);
      const voiceChannel = this.bot.channels.cache.get(user.voiceChannelID);

      if (serverID && user.voiceChannelID) {
        // resolve(`has started a screenshare! Join the voice channel \`${voiceChannel.name}\` and click this link to join:\n<https://discordapp.com/channels/${serverID}/${user.voiceChannelID}>`);

        let embedObj = {
          type: 'rich',
          title: `${user.user.username} has invited everyone to a screenshare session!`,
          description: `Join \`🔈 ${voiceChannel.name}\` and then [click here](https://discordapp.com/channels/${serverID}/${user.voiceChannelID}) afterward to participate!`,
          color: 0x7affdc,
          fields: []
        };

        resolve({
          to: bundle.channelID,
          embed: embedObj
        });
      } else {
        resolve('You are not in a voice channel.');
      }
    });
  }
};

module.exports = Command;
