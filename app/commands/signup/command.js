const BasicCommand = use('classes/BasicCommand');

/**
 * @alias Command.signup
 * @extends _.BasicCommand
 */
class Command extends BasicCommand {
  /**
   * code to run on load
   */
  init() {
    this._auriga.loadService('discord').then((discord) => {
      this.discord = discord;
      this.bot     = discord.bot;
      this.storage = this._auriga.storage;
      this.cache   = [];
      this.initializeCache()
      .then(this.cacheSettingsChannel.bind(this))
      .then(() => {
        this.hookReactions();
        this.register();
      });
    });
  }

  /**
   * loads the channel required for notification control into the cache
   * @return {promise} resolves with response when finished, rejects on errors
   */
  cacheSettingsChannel() {
    return new Promise((resolve, reject) => {
      this.bot.channels.fetch(this._settings.channel)
      .then(resolve)
      .catch(resolve);
    });
  }
  /**
   * hooks into the reaction add/remove to see if we are reacting with a bot
   * provided signup message. If so, it'll process the reaction as necessary
   */
  hookReactions() {
    /* requires raw event:
      {
        t: 'MESSAGE_REACTION_ADD',
        s: 15,
        op: 0,
        d: {
          user_id: '84484653687267328',
          message_id: '447739338655268864',
          emoji: {
            name: '👌',
            id: null,
            animated: false
          },
          channel_id: '254574550816129024',
          guild_id: '174672265953148929'
        }
      }
    */

    this.bot.on('raw', event => {
      let action = false;
      switch(event.t) {
        case 'MESSAGE_REACTION_ADD':
          action = 'add';
          break;
        case 'MESSAGE_REACTION_REMOVE':
          action = 'remove';
          break;
      }

      if (!action) {
        return;
      }

      // get the role associated to this message from the db
      this.storage.findOne({
        type: 'signup-message',
        serverID: event.d.guild_id,
        channelID: event.d.channel_id,
        messageID: event.d.message_id
      }, (err, doc) => {
        if (!err) {
          let guild = this.bot.guilds.cache.get(event.d.guild_id);
          guild.members.fetch(event.d.user_id, true)
          .then(member => {
            let role = guild.roles.cache.find(role => role.name === `🚩 ${doc.role}`);
            if (action === 'add' && role) {
              member.roles.add(role);
            } else if (role) {
              member.roles.remove(role);
            }
          });
        }
      });
    });
  }

  /**
   * runs the command code when called
   * @param {string} message the message received from the service
   * @param {object} bundle the bundled meta data provided by the service
   * @param {string} service the service that called the action
   * @return {promise} resolves with response when finished, rejects on errors
   */
  action(message, bundle, service) {
    if (service._name !== 'discord') {
      return Promise.reject();
    }

    return new Promise((resolve, reject) => {
      const messageParts = this._auriga.extractCommandFromString(message);
      if (messageParts.str.trim().length < 1) {
        resolve('Please complete the command with \`add\`/\`remove\` and the signup group name');
      } else {
        let channel;
        if (this._settings.channel) {
          channel = this.bot.channels.cache.get(this._settings.channel);
        } else {
          channel = this.bot.channels.cache.get(bundle.channelID);
        }
        const guild   = channel.guild;
        let payload = {
          channel: channel,
          guild  : guild,
          name   : messageParts.str
        };

        switch(messageParts.cmd.toLowerCase()) {
          case 'add':
            this.createRole(payload)
            .then(this.createMessage.bind(this))
            .then(this.addEmojiResponse.bind(this))
            .then(this.saveSignupToDatabase.bind(this))
            .then(this.addDocToCache.bind(this))
            .then(resolve('Signup group added'))
            .catch(resolve);
            break;
          case 'remove':
            this.deleteRole(payload)
            .then(this.getSignupFromDatabase.bind(this))
            .then(this.deleteMessage.bind(this))
            .then(this.deleteRoleStorage.bind(this))
            .then(resolve('Signup group deleted if it existed'))
            .catch(resolve);
            break;
          default:
            resolve(`you must specify \`add\` or \`remove\``);
            break;
        }
      }
    });
  }

  /**
   * removes a role from local cache and storage
   * @param {object} payload an object containing parameters
   * @param {object} payload.channel the text channel
   * @param {object} payload.guild the guild
   * @param {string} payload.name the role name
   * @return {promise} resolves with payload when finished, rejects on errors
   */
  deleteRoleStorage(payload) {
    return new Promise((resolve, reject) => {
      this.removeDocFromCache(payload)
      .then(() => {
        this.storage.remove({
          type:'signup-message',
          serverID:payload.guild.id,
          channelID:payload.channel.id,
          role: payload.name
        });
      })
      .catch(resolve);
    });
  }

  /**
   * deletes the signup role
   * @param {object} payload an object containing parameters
   * @param {object} payload.channel the text channel
   * @param {object} payload.guild the guild
   * @param {string} payload.name the role name
   * @return {promise} resolves with payload when finished, rejects on errors
   */
  deleteRole(payload) {
    return new Promise((resolve, reject) => {
      let role = payload.guild.roles.cache.find(x => x.name === `🚩 ${payload.name}`);
      if (role) {
        role.delete();
      }
      resolve(payload);
    });
  }

  /**
   * creates the signup role
   * @param {object} payload an object containing parameters
   * @param {object} payload.channel the text channel
   * @param {object} payload.guild the guild
   * @param {string} payload.name the role name
   * @return {promise} resolves with payload when finished, rejects on errors
   */
  createRole(payload) {
    return new Promise((resolve, reject) => {
      let role = payload.guild.roles.cache.find(x => x.name === `🚩 ${payload.name}`);
      if (!role) {
        payload.guild.roles.create({
          data: {
            name: `🚩 ${payload.name}`,
            mentionable: true
          },
          reason: `bot signup creation`
        })
        .then(() => {
          payload.role = payload.guild.roles.cache.find(x => x.name === `🚩 ${payload.name}`);
          resolve(payload);
        })
        .catch(resolve);
      } else {
        reject(`The signup group \`🚩 ${payload.name}\` already exists`);
      }
    });
  }

  /**
   * deletes the message for signup
   * @param {object} payload an object containing parameters
   * @param {object} payload.channel the text channel
   * @param {object} payload.guild the guild
   * @param {string} payload.name the role name
   * @param {string} payload.signup the signup object loaded from database
   * @return {promise} resolves with payload when finished, rejects on errors
   */
  deleteMessage(payload) {
    return new Promise((resolve, reject) => {
      payload.channel.messages.fetch(payload.signup.messageID)
      .then((msg) => {
        msg.delete();
        resolve(payload);
      })
      .catch(resolve(payload));
    });
  }

  /**
   * creates the message for users to react to for signup
   * @param {object} payload an object containing parameters
   * @param {object} payload.channel the text channel
   * @param {object} payload.guild the guild
   * @param {string} payload.name the role name
   * @return {promise} resolves with payload when finished, rejects on errors
   */
  createMessage(payload) {
    return new Promise((resolve, reject) => {
      payload.channel.send(`**${payload.name}**`)
      .then((message) => {
        payload.message = message;
        resolve(payload);
      })
      .catch(reject);
    });
  }

  /**
   * creates the initial reaction that users can interact with
   * @param {object} payload an object containing parameters
   * @param {object} payload.channel the text channel
   * @param {object} payload.guild the guild
   * @param {string} payload.name the role name
   * @param {string} payload.message the message the bot created
   * @return {promise} resolves with payload when finished, rejects on errors
   */
  addEmojiResponse(payload) {
    return new Promise((resolve, reject) => {
      payload.message.react('🚩')
      .then(resolve(payload))
      .catch(reject);
    });
  }

  /**
   * saves the signup message details to the database
   * @param {object} payload an object containing parameters
   * @param {object} payload.channel the text channel
   * @param {object} payload.guild the guild
   * @param {string} payload.name the role name
   * @param {string} payload.message the message the bot created
   * @return {promise} resolves with payload when finished, rejects on errors
   */
  saveSignupToDatabase(payload) {
    return new Promise((resolve, reject) => {
      let doc = {
        type: 'signup-message',
        serverID: payload.guild.id,
        channelID: payload.channel.id,
        messageID: payload.message.id,
        role: payload.name
      };

      this.storage.update(
        {
          type: 'signup-message',
          serverID: payload.guild.id,
          channelID: payload.channel.id,
          role: payload.name
        }, doc, {upsert:true}, (err, resp) => {
        if (err) {
          reject(err);
        } else {
          resolve(payload);
        }
      });
    });
  }

  /**
   * adds the matching message id to the payload
   * @param {object} payload an object containing parameters
   * @param {object} payload.channel the text channel
   * @param {object} payload.guild the guild
   * @param {string} payload.name the role name
   * @return {promise} resolves with payload when finished, rejects on errors
   */
  getSignupFromDatabase(payload) {
    return new Promise((resolve, reject) => {
      this.storage.findOne({
        type:'signup-message',
        serverID: payload.guild.id,
        channelID: payload.channel.id,
        role: payload.name
      }, (err, doc) => {
        if (!err) {
          payload.signup = doc;
        }
        resolve(payload);
      });
    });
  }

  /**
   * puts the signup details into local cache
   * @param {object} payload an object containing parameters
   * @param {object} payload.channel the text channel
   * @param {object} payload.guild the guild
   * @param {string} payload.name the role name
   * @param {string} payload.message the message the bot created
   * @return {promise} resolves with payload when finished, rejects on errors
   */
  addDocToCache(payload) {
    return new Promise((resolve, reject) => {
      this.removeDocFromCache(payload)
      .then(() => {
        this.cache.push({
          type: 'signup-message',
          serverID: payload.guild.id,
          channelID: payload.channel.id,
          messageID: payload.message.id,
          role: payload.name
        });
        resolve(payload);
      });
    });
  }

  /**
   * removes signup details from local cache
   * @param {object} payload an object containing parameters
   * @param {object} payload.channel the text channel
   * @param {object} payload.guild the guild
   * @param {string} payload.name the role name
   * @param {string} payload.message the message the bot created
   * @return {promise} resolves with payload when finished, rejects on errors
   */
  removeDocFromCache(payload) {
    return new Promise((resolve, reject) => {
      let foundIndex = this.cache.findIndex((o) => {
        return o.role === payload.name;
      });
      if (foundIndex !== -1) {
        this.cache.splice(foundIndex, 1);
      }
      resolve(payload);
    });
  }

  /**
   * loads all documents into the cache
   * @return {promise} resolves with when finished, rejects on errors
   */
  initializeCache() {
    return new Promise((resolve, reject) => {
      this.cache = [];
      this.storage.find({type:'signup-message'}, (err, docs) => {
        if (err) {
          reject(err);
        } else {
          docs.forEach((doc) => {
            this.cache.push(doc);
          });

          resolve();
        }
      });
    });
  }
};

module.exports = Command;
