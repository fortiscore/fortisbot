const BasicCommand = use('classes/BasicCommand');

/**
 * @alias Command.source
 * @extends _.BasicCommand
 */
class Command extends BasicCommand {
  /**
   * code to run on load
   */
  init() {
    Promise.all([
      this._auriga.loadService('discord')
    ]).then((requirements) => {
      this.discord = requirements[0];
      this.register();
    });
  }

  /**
   * runs the command code when called
   * @param {string} message the message received from the service
   * @param {object} bundle the bundled meta data provided by the service
   * @param {string} service the service that called the action
   * @return {promise} resolves with response when finished, rejects on errors
   */
  action(message, bundle, service) {
    return new Promise((resolve, reject) => {
      // only support discord for now
      if (service._name !== 'discord') {
        reject(this.error(`unsupported service: ${service._name}`));
      }

      resolve({
        to: bundle.channelID,
        embed: {
          title: 'Frosthaven/auriga.io (branch/master)',
          description: `auriga.io is a platform agnostic community bot that provides users with commands that they can run. Initially built for support with twitch chat and discord chat, auriga.io can take you as far as you want it to with its extendable class-inheritance based modular design and tight permission controls.`,
          url: 'https://bitbucket.org/Frosthaven/auriga.io',
          color: 0x205081,
          thumbnail: {
            url: 'https://puu.sh/sESrz/95206f8195.png'
          }
        }
      });
    });
  }
};

module.exports = Command;
