const BasicCommand = use('classes/BasicCommand');
const moment       = require('moment');
const shortNumber  = require('short-number');

/**
 * @alias Command.stats
 * @extends _.BasicCommand
 */
class Command extends BasicCommand {
  /**
   * code to run on load
   */
  init() {
    Promise.all([
      this._auriga.loadService('discord'),
      this._auriga.loadPlugin('discordStats')
    ]).then((requirements) => {
      this.discord = requirements[0];
      this.stats   = requirements[1];
      this.register();
    });
  }

  /**
   * runs the command code when called
   * @param {string} message the message received from the service
   * @param {object} bundle the bundled meta data provided by the service
   * @param {string} service the service that called the action
   * @return {promise} resolves with response when finished, rejects on errors
   */
  action(message, bundle, service) {
    return new Promise((resolve, reject) => {
      // only support discord for now
      if (service._name !== 'discord') {
        reject(this.error(`unsupported service: ${service._name}`));
      }

      // don't support direct messages
      let channel = this.discord.bot.channels.cache.get(bundle.channelID);
      if (!channel || !channel.guild || !channel.guild.id) {
        resolve('This must be run from within a valid server');
      }

      // verify server exists and stats plugin is loaded
      const serverID = channel.guild.id;
      if (!this.stats || !serverID) {
        reject(this.error(`Dependancies not met or server doesn't exist`));
      }

      // handle provided parameters
      let userID;
      let params = message.trim();
      if (params.startsWith('--leaderboard')) {
        let age   = 14;
        let limit = 15;
        this.stats.getTopPosters(serverID, limit, age)
        .then((entries) => {
          let count = 0;
          let results = entries.map((x) => {
            if (x.name) {
              count++;
              return `\`${count}. ${x.name} (${x.messages_sent})\``;
            }
          });
          resolve(`Here are the top ${count} users that have posted within the last ${age} days:\n${results.join('\n')}`);
        }).catch(resolve);
      } else if (params.startsWith('--purge')) {
        userID = bundle.userID;

        let tokens = params.split(' ');
        tokens.shift();
        let target = tokens.join(' ');
        if (target.length <= 0) {
          // full purge
          this.stats.purgeAppHistory(userID, serverID)
          .then((stats) => {
            resolve(`Your application history has been reset!`);
          }).catch(resolve);
        } else {
          // purge only the target
          this.stats.purgeSingleApp(userID, serverID, target)
          .then((stats) => {
            resolve(`Removed \`🎮 ${target}\` from your application history!`);
          }).catch(resolve);
        }
      } else {
        // fetching stats for the user, lets pull from discord directly since
        // discord.js doesn't always cache every user

        let guild = this.discord.bot.guilds.cache.get(serverID);
        guild.members.fetch(userID, true).then(myUser => {
          if (message === '') {
            userID = bundle.userID;
          } else {
            userID = this.discord.resolveUserID(serverID, message);
          }

          if (!userID) {
            // can we get the userID from cache using last_known_as?
            let nameSearch = new RegExp(message.toLowerCase().trim(), 'i');
            this._auriga.storage.findOne({
              type: 'user-stats',
              serverID: serverID,
              last_known_as: nameSearch
            }, (err, doc) => {
              if (err || !doc) {
                resolve(`Couldn't find any matching users`);
              } else {
                // run the action
                this.processAction(doc.userID, serverID, bundle, resolve, doc);
              }
            });
          } else {
            // run the action
            this.processAction(userID, serverID, bundle, resolve);
          }
        });
      }
    });
  }

  /**
   * processes the action for fetching the user and building a response
   * @param {string} userID the user's discord id
   * @param {string} serverID the server id
   * @param {object} bundle the bundle provided to the command
   * @param {function} resolve the original resolve function
   * @param {object} cached optional, cached information if provided by storage
   */
  processAction(userID, serverID, bundle, resolve, cached) {
    this.stats.getUserStats(userID, serverID)
    .then((stats) => {
      const response = this.buildResponse(stats, bundle, cached);
      response.message = (response.message)
                        ? `<@${bundle.userID}>: ${response.message}`
                        : `<@${bundle.userID}>: \n`;
      resolve(response);
    });
  }

  /**
   * builds a stats response for discord
   * @param {object} stats the user stats provided by discordStats plugin
   * @param {object} bundle the bundle provided to the command
   * @param {object} cached optional, cached information if provided by storage
   * @return {object} a response object that can be sent to the discord service
   */
  buildResponse(stats, bundle, cached) {
    const bot   = this.stats.bot;
    const guild = bot.guilds.cache.get(stats.serverID);
    let user;

    if (cached) {
      user = {
        id: cached.userID,
        displayName: cached.last_known_as,
        trackingSince: cached.tracking_since,
        userTypeLabel: 'cached user',
        avatar: false
      };
    } else {
      const guildUser = (guild) ? guild.members.cache.get(stats.userID) : null;

      if (!guildUser) {
        return {
          to: bundle.channelID,
          message: 'Requested user couldn\'t be found.'
        };
      }

      user = {
        id: guildUser.id,
        displayName: this.discord.getDisplayNameByID(guild.id, guildUser.id),
        trackingSince: this.getTrackingSinceForUser(stats, bundle),
        userTypeLabel: (guildUser.user.bot) ? 'bot' : 'user',
        avatar: guildUser.user.avatar
      };
    }

    // set display variables
    bundle.isBot          = (user.userTypeLabel === 'bot') ? true : false;

    // get avatar url
    let avatarUrl = '';
    if (user.avatar) {
      const avatarExtension = (user.avatar.startsWith('a_'))
                            ? 'gif'
                            : 'png';
      avatarUrl = `https://cdn.discordapp.com/avatars/${stats.userID}/${user.avatar}.${avatarExtension}`;
    }

    // configure the base embed
    let embedObj = {
      type: 'rich',
      title: `${user.displayName} of ${guild.name}`,
      color: 0xff63a2,
      thumbnail: {
        url: avatarUrl
      }
    };

    // add description
    if (user.trackingSince) {
      embedObj.description = `Tracking for this ${user.userTypeLabel} started at least ${user.trackingSince.fromNow()}`;
    } else {
      embedObj.description = `Displaying cached data for this user (live stats unavailable)`;
    }

    // configure embed fields
    embedObj.fields = this.buildResponseFields(stats, bundle);

    return {
      to: bundle.channelID,
      embed: embedObj
    };
  }

  /**
   * gets how long we've tracked the user
   * @param {object} stats the user stats provided by discordStats plugin
   * @param {object} bundle the bundle provided to the command
   * @return {object} a moment object representing when we started tracking
   */
  getTrackingSinceForUser(stats, bundle) {
    const bot                = this.stats.bot;
    const trackingSinceStamp = this.stats.globalStats.tracking_since;
    const guild              = bot.guilds.cache.get(stats.serverID);
    const member             = guild.members.cache.get(stats.userID);
    const botMember          = guild.members.cache.get(bot.user.id);

    // set timestamps
    const trackTimeM  = moment.utc(trackingSinceStamp);
    const botJoinedM  = moment.utc(botMember.joinedTimestamp);
    const userJoinedM = moment.utc(member.joinedTimestamp);
    let trackingSince = trackTimeM;
    if (botJoinedM > trackTimeM) {
      // bot joined this server after tracking was enabled
      trackingSince = botJoinedM;
    }
    if (userJoinedM > botJoinedM && userJoinedM > trackTimeM) {
      // user joined after tracking was enabled and after the bot was added
      trackingSince = userJoinedM;
    }

    return trackingSince;
  }

  /**
   * builds fields for the discord embed response
   * @param {object} stats the user stats provided by discordStats plugin
   * @param {object} bundle the bundle provided to the command
   * @return {array} an array of fields
   */
  buildResponseFields(stats, bundle) {
    let fields = [
      this.buildFieldMessageCounter(stats, bundle),
      this.buildFieldStatus(stats, bundle),
      this.buildFieldLastInteraction(stats, bundle),
      this.buildFieldHistorical(stats, bundle)
    ];

    if (!bundle.isBot) {
      if (
        this._auriga.config.commands.group &&
        this._auriga.config.commands.group.enabled
      ) {
        fields.push(this.buildFieldGroups(stats, bundle));
      }
      fields.push(this.buildFieldApplicationHistory(stats, bundle));
    }
    return fields;
  }

  /**
   * builds a field entry
   * @param {object} stats the user stats provided by discordStats plugin
   * @param {object} bundle the bundle provided to the command
   * @return {object} the field to return
   */
  buildFieldMessageCounter(stats, bundle) {
    // message counter and rank field
    const rank = this.stats.getRankFromMessageCount(
      stats.serverID,
      stats.messages_sent
    );

    let name = 'Messaging Stats';
    let value = '';

    if (typeof rank === 'object' && rank && rank.rank && !bundle.isBot) {
      name  = `Messaging Rank`;
      value = `${rank.rank} (${shortNumber(rank.postReq)}+)\n- Rank ${rank.numeric} of ${rank.total}\n- Sent ${stats.messages_sent} messages`;
    } else {
      value = `Sent ${stats.messages_sent} messages`;
    }

    return {
      name: name,
      value: value
    };
  }

  /**
   * builds a field entry
   * @param {object} stats the user stats provided by discordStats plugin
   * @param {object} bundle the bundle provided to the command
   * @return {object} the field to return
   */
  buildFieldStatus(stats, bundle) {
    let value = '';

    const guild  = this.stats.bot.guilds.cache.get(stats.serverID);
    const member = guild.members.cache.get(stats.userID);

    if (member) {
      const status = member.presence.status;
      const game   = (
                      member.presence.activity &&
                      member.presence.activity.type === 'PLAYING' &&
                      member.presence.activity.url === null
                     )
                   ? member.presence.activity.name
                   : null;
      // game status
      if (game) {
        // playing a game
        value += `\`🎮 ${game.replace(/\🎮/g, '')}\`\n`;
      }

      // online status
      if (status) {
        // just a normal status
        if (stats.last_status_time) {
          const statusM = moment.utc(stats.last_status_time);
          value += `${status} for ${statusM.toNow(true)}`;
        } else {
          value += status;
        }
      } else {
        value += 'offline';
      }

      return {
        name: 'Status',
        value: value,
        inline: true
      };
    } else {

      return {
        name: 'Status',
        value: 'unknown',
        inline: true
      };
    }
  }

  /**
   * builds a field entry
   * @param {object} stats the user stats provided by discordStats plugin
   * @param {object} bundle the bundle provided to the command
   * @return {object} the field to return
   */
  buildFieldLastInteraction(stats, bundle) {
    let lastSpokeM;
    let lastInteraction;

    if (stats.userID === bundle.userID) {
      lastInteraction = 'just now';
    } else if (stats.last_spoke) {
        lastSpokeM = moment.utc(stats.last_spoke);
        lastInteraction = lastSpokeM.fromNow();
    } else {
        lastInteraction = 'unknown';
    }

    return {
      name: 'Last Interaction',
      value: lastInteraction,
      inline: true
    };
  }

  /**
   * builds a field entry
   * @param {object} stats the user stats provided by discordStats plugin
   * @param {object} bundle the bundle provided to the command
   * @return {object} the field to return
   */
  buildFieldHistorical(stats, bundle) {
    const guild = this.stats.bot.guilds.cache.get(stats.serverID);
    const member = guild.members.cache.get(stats.userID);

    let embedField = {
      name: 'Historical',
      inline: true
    };

    const createdStamp = new Date(
      parseInt(stats.userID) / 4194304 + 1420070400000
    );
    const createdM = moment.utc(createdStamp);

    if (member) {
      const joinedM = moment.utc(member.joinedTimestamp);
      embedField.value = `Created: ${createdM.fromNow()}\nJoined:    ${joinedM.fromNow()}`;
    } else {
      embedField.value = `Created: ${createdM.fromNow()}`;
    }

    return embedField;
  }

  /**
   * builds a field entry
   * @param {object} stats the user stats provided by discordStats plugin
   * @param {object} bundle the bundle provided to the command
   * @return {object} the field to return
   */
  buildFieldGroups(stats, bundle) {
    let val = '`N/A`';
    let guild = this.discord.bot.guilds.cache.get(stats.serverID);
    const member = guild.members.cache.get(stats.userID);
    const groups = member.roles.cache.filter((x) => {
      return x.name.startsWith('⚑');
    }).map((x) => {
      return `\`${x.name}\``;
    });

    if (groups.length > 0) {
      val = groups.join(' ');
    }

    return {
      name: 'Groups',
      value: val
    };
  }

  /**
   * builds a field entry
   * @param {object} stats the user stats provided by discordStats plugin
   * @param {object} bundle the bundle provided to the command
   * @return {object} the field to return
   */
  buildFieldApplicationHistory(stats, bundle) {
    const maxRemembered = this.stats._settings.gameHistorySize || 10;
    let gameHistory = stats.game_history || [];
    let displayHistory = gameHistory.map((x) => {
      return `\`🎮 ${x.replace(/\🎮/g, '')}\``;
    });

    if (displayHistory.length > maxRemembered) {
      const difference = displayHistory.length - maxRemembered;
      displayHistory.splice(0, difference);
    }

    displayHistory = displayHistory.reverse().join(' ');

    if (displayHistory.length === 0) {
      displayHistory = '`N/A`';
    }

    return {
      name: 'Application History',
      value: displayHistory
    };
  }
};

module.exports = Command;
