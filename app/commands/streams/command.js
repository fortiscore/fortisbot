const BasicCommand = use('classes/BasicCommand');

/**
 * @alias Command.coin
 * @extends _.BasicCommand
 */
class Command extends BasicCommand {
  /**
   * code to run on load
   */
  init() {
    Promise.all([
      this._auriga.loadCommand('streamreg')
    ]).then((requirements) => {
      this.storage = this._auriga.storage;
      this.register();
    });
  }

  /**
   * runs the command code when called
   * @param {string} message the message received from the service
   * @param {object} bundle the bundled meta data provided by the service
   * @param {string} service the service that called the action
   * @return {promise} resolves with response when finished, rejects on errors
   */
  action(message, bundle, service) {
    return new Promise((resolve, reject) => {
      // only support discord for now
      if (service._name !== 'discord') {
        reject(this.error(`unsupported service: ${service._name}`));
      }

      this.getLiveStreams()
      .then((streams) => {
        let streamList = '';
        streams.forEach((stream) => {
          streamList += `\n\`${stream.display_name}\` is playing \`${stream.game}\`: ${stream.status}\n<${stream.url}>\n`;
        });

        const output = `:red_circle: The following streams are currently online:\n${streamList}`;
        resolve(output);
      })
      .catch(resolve);
    });
  }

  /**
   * gets a list of active live streams saved in the stream registry
   * @return {promise} resolves with streams on success, rejects on errors
   */
  getLiveStreams() {
    return new Promise((resolve, reject) => {
      this.storage.find({'type':'stream-registry', 'online':true}, (e, arr) => {
        if (e || arr.length <= 0) {
          reject(this._settings.no_streams_response);
        } else {
          arr.sort((a, b) => {
            const nameA = a.name.toUpperCase();
            const nameB = b.name.toUpperCase();
            if (nameA < nameB) {
              return -1;
            }
            if (nameA > nameB) {
              return 1;
            }
            return 0;
          });

          resolve(arr);
        }
      });
    });
  }
};

module.exports = Command;
