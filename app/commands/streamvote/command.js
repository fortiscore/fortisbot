const BasicCommand = use('classes/BasicCommand');

/**
 * @alias Command.streamvote
 * @extends _.BasicCommand
 */
class Command extends BasicCommand {
  /**
   * code to run on load
   */
  init() {
    this._auriga.loadPlugin('streamVoting').then((streamVoting) => {
      this.streamVoting = streamVoting;
      this.defaultDuration = 60;
      this.register();
    });
  }

  /**
   * runs the command code when called
   * @param {string} message the message received from the service
   * @param {object} bundle the bundled meta data provided by the service
   * @param {object} service the service that called the action
   * @return {promise} resolves with response when finished, rejects on errors
   */
  action(message, bundle, service) {
    return new Promise((resolve, reject) => {
      if (service._name !== 'twitch') {
        reject();
      }

      this.parseVoteCommand(message).then((data) => {
        this.streamVoting.openPoll(bundle.channel, data);
      }).catch(resolve);
    });
  }

  /**
   * Parses a vote command string into usable data
   * @param {string} message the command string
   * @return {promise} resolves on completion, rejects on errors
   */
  parseVoteCommand(message) {
    return new Promise((resolve, reject) => {
      const regexp = /(?:\*{1})([^*]+)/g;
      let options = [];
      let result  = null;
      while((result = regexp.exec(message)) !== null) {
        options.push(result[1].trim());
      }
      if (options.length > 1) {
        let data = {
          text: message.substring(0, message.indexOf('*')).trim(),
          options: options,
          duration: this.defaultDuration
        };
        resolve(data);
      } else {
        reject('You didn\'t provide enough poll options!');
      }
    });
  }

};

module.exports = Command;
