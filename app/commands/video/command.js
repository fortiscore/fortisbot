const BasicCommand = use('classes/BasicCommand');
const youtube      = require('youtube-search');

/**
 * @alias Command.video
 * @extends _.BasicCommand
 */
class Command extends BasicCommand {
  /**
   * runs the plugin code when called
   */
  init() {
    Promise.all([
      this._auriga.loadPlugin('googleApi')
    ]).then((requirements) => {
      this.google = requirements[0];
      this.register();
    });
  }

  /**
   * runs the command code when called
   * @param {string} message the message received from the service
   * @param {object} bundle the bundled meta data provided by the service
   * @param {string} service the service that called the action
   * @return {promise} resolves with response when finished, rejects on errors
   */
  action(message, bundle, service) {
    return new Promise((resolve, reject) => {
      if (service._name !== 'discord') {
        reject();
      }

      this.google.youtubeSearch(message, {maxResults: 20})
      .then((results) => {
        const url = results[Math.floor(Math.random()*results.length)].link;
        resolve(`:video_camera: ${url}`);
      }).catch((err) => {
        this.warn(err);
        resolve(`:video_camera: ${err}`);
      });
    });
  }
};

module.exports = Command;
