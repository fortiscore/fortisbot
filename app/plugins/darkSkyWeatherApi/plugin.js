const BasicPlugin   = use('classes/BasicPlugin');
const PromiseRunner = use('classes/PromiseRunner');
const nodeGeocoder  = require('node-geocoder');
const request       = require('request');

/**
 * @alias Plugin.darkSkyWeatherApi
 * @extends _.BasicPlugin
 */
class Plugin extends BasicPlugin {

  /**
   * concatenates summary text for saving display space
   * @param {string} input the full weather summary sentence
   * @return {string} the shortened weather summary text
   */
  concatSummary(input) {
    let words = input.split(' ');
    let shortened;

    if (
      words[1]
      && (
        words[1].startsWith('(')
        || words[1].startsWith('in')
        || words[1].startsWith('starting')
        || words[1].startsWith('until')
        || words[1].startsWith('throughout')
      )
    ) {
      shortened = words.splice(0, 1).join(' ');
    } else {
      shortened = words.splice(0, 2).join(' ');
    }

    return shortened;
  }

  /**
   * fetches weather data using the darksky api
   * @param {string} query the text query for location (city/state/zip/etc)
   * @return {promise} resolves on success, rejects on errors
   */
  fetchWeather(query) {
    let that = this;
    return new Promise((parentResolve, parentReject) => {
      let geocoder = nodeGeocoder({
        provider: 'openstreetmap'
      });

      new PromiseRunner(3, 0, () => {
        return new Promise((resolve, reject) => {
          geocoder.geocode(query, (err, response) => {
            if (err) {
              return reject(err);
            } else if (!response || !response[0]) {
              return reject('Couldn\'t find that geolocation');
            }

            const address = response[0].formattedAddress;
            const lat     = response[0].latitude;
            const lng     = response[0].longitude;
            const url     = `https://api.darksky.net/forecast/${this._settings.api_key}/${lat},${lng}`;
            request(url, (error, response, body) => {
              if (!error && response.statusCode === 200 && response.body) {
                const data = JSON.parse(response.body);
                data.address = address;
                return resolve(data);
              } else if (error) {
                return reject(`Failed to fetch weather for ${address}`);
              } else {
                return reject(`Failed to fetch weather for ${address}`);
              }
            });
          });
        });
      }).go().then((result) => {
        parentResolve(result);
      }).catch((err) => {
        that.warn(err);
        parentReject(`${e.last} after ${e.all.length} tries.`);
      });
    });
  }
};

module.exports = Plugin;
