const BasicPlugin = use('classes/BasicPlugin');

/**
 * @alias Plugin.discordForumBumping
 * @extends _.BasicPlugin
 */
class Plugin extends BasicPlugin {
  /**
   * runs the plugin code when called
   */
  init() {
    this._auriga.loadService('discord').then((discord) => {
      this.bot          = null;
      this.discord = discord;
      this.getDiscordBot()
        .then(this.watchMessages.bind(this))
        .then(() => {
          this.register();
        })
        .catch(this.onError.bind(this));
    });
  }

  /**
   * gets the discord bot when available
   * @return {promise} resolves with the discord bot
   */
  getDiscordBot() {
    return new Promise((resolve, reject) => {
      this._auriga.loadService('discord').then((discord) => {
        this.bot = discord.bot;
        resolve();
      }).catch((x) => reject(x));
    });
  }

  /**
   * watches new messages for bumping
   * @return {promise} resolves when completed
   */
  watchMessages() {
    return new Promise((resolve, reject) => {
      this.bot.on('message', msg => {
        const isDirectMessage = (msg.channel.type === 'dm');
        if (!isDirectMessage) {
          const channel   = this.bot.channels.cache.get(msg.channel.id);
          const parentID  = channel.parentID || false;
          if (
            this._settings.forum_categories &&
            this._settings.forum_categories.includes(parentID)
          ) {
            channel.setPosition(0);
          }
        }
      });

      resolve();
    });
  }

  /**
   * error handler
   * @param {string} e the error message
   */
  onError(e) {
    this._auriga.warn(e);
  }
};

module.exports = Plugin;
