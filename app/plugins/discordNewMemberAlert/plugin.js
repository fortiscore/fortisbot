const BasicPlugin = use('classes/BasicPlugin');

/**
 * @alias Plugin.discordNewMemberAlert
 * @extends _.BasicPlugin
 */
class Plugin extends BasicPlugin {
  /**
   * runs the plugin code when called
   */
  init() {
    Promise.all([
      this._auriga.loadService('discord')
    ]).then((requirements) => {
      this.discord = requirements[0];

      this.registerAlert();
      this.register();
    });
  }

  /**
   * hooks into the new member event on discord and responds
   */
  registerAlert() {
    this.discord.bot.on('guildMemberAdd', member => {
      const guildJoinedID = member.guild.id;
      const alertSettings = this._settings[guildJoinedID];

      const userID = member.user.id;
      const isBot  = member.user.bot;
      const announceto = this._settings[guildJoinedID].announceto
                         || guildJoinedID;

      if (!isBot && alertSettings) {
        // send global announcement
        if (alertSettings.announce) {
          this.discord.respond({
            channelID: announceto,
            response: {
              to: announceto,
              message: this.prepareString(userID, alertSettings.announce)
            }
          });
        }

        // send private dm to user
        this.discord.respond({
          channelID: userID,
          response: {
            to: userID,
            message: this.prepareString(userID, alertSettings.dm)
          }
        });
      }
    });

    return;
  }

  /**
   * prepares message strings with replacements
   * @param {string} userID the user id of the member who joined
   * @param {string} str the message string to run replacements on
   * @return {string} the transformed string
   */
  prepareString(userID, str) {
    str = str
          .replace(/\%user/g, `<@${userID}>`)
          .replace(/\%here/g, '@here')
          .replace(/\%help/g, `${this._auriga.config.prefix}help`);
    return str;
  }
};

module.exports = Plugin;

