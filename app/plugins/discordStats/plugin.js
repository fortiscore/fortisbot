const BasicPlugin = use('classes/BasicPlugin');
const moment      = require('moment');

/**
 * @alias Plugin.discordStats
 * @extends _.BasicPlugin
 */
class Plugin extends BasicPlugin {
  /**
   * runs the plugin code when called
   */
  init() {
    this._auriga.loadService('discord').then((discord) => {
      this.storage      = this._auriga.storage;
      this.globalStats  = null;
      this.bot          = null;
      this.discord = discord;
      this.loadGlobalStats()
        .then(this.getDiscordBot.bind(this))
        .then(this.watchPresence.bind(this))
        .then(this.watchMessages.bind(this))
        .then(() => {
          this.register();
        })
        .catch(this.onError.bind(this));
    });
  }

  /**
   * gets the discord bot when available
   * @return {promise} resolves with the discord bot
   */
  getDiscordBot() {
    return new Promise((resolve, reject) => {
      this._auriga.loadService('discord').then((discord) => {
        this.bot = discord.bot;
        resolve();
      }).catch((err) => {
        this.onError(err);
        reject(err);
      });
    });
  }

  /**
   * gets the current game title being played in a presence
   * @param {object} presence the presence object
   * @return {string} the game title being played OR null
   */
  getCurrentGameFromPresence(presence) {
    let gameTitle = null;
    if (presence.game && presence.game.name && presence.game.url === null) {
      gameTitle = presence.game.name;
    }
    return gameTitle;
  }

  /**
   * watches presence changes
   * @return {promise} resolves when completed
   */
  watchPresence() {
    return new Promise((resolve, reject) => {
      this.bot.on('presenceUpdate', (olduser, newuser) => {
        const presence  = newuser.activities[0];
        const game      = (
                            presence &&
                            presence.type === 'PLAYING' &&
                            presence.type !== 'CUSTOM_STATUS' &&
                            presence.url === null
                          )
                        ? presence.name
                        : null;

        this.getUserStats(newuser.userID, newuser.guild.id).then(stats => {
          stats = this.decorateStatsWithGameHistory(stats, game);
          stats = this.decorateStatsWithActivity(stats, newuser.status);
          this.updateUserStats(stats);
        });
      });

      resolve();
    });
  }

  /**
   * watches new messages
   * @return {promise} resolves when completed
   */
  watchMessages() {
    return new Promise((resolve, reject) => {
      this.bot.on('message', msg => {
        const isDirectMessage = (msg.channel.type === 'dm');
        if (!isDirectMessage) {
          const serverID = this.bot.channels.cache.get(msg.channel.id).guild.id;
          this.getUserStats(msg.author.id, serverID).then(stats => {
            stats = this.decorateStatsWithCounters(stats, msg.content);
            this.handleMessagingRanks(stats, msg.channel.id).then((stats) => {
              this.updateUserStats(stats);
            });
          });
        }
      });

      resolve();
    });
  }

  /**
   * fetches global stats from the database, creating it if it doesn't exist
   * @return {promise} resolves with the global stats object
   */
  loadGlobalStats() {
    const storage = this.storage;
    const self    = this;

    return new Promise((resolve, reject) => {
      storage.findOne({type: 'global-stats'}, function(err, globalStats) {
        if (err) {
          reject(err);
        } else if (globalStats) {
          self.globalStats = globalStats;
          resolve(globalStats);
        } else {
          // we need to create the global stats
          globalStats = {
            type: 'global-stats',
            tracking_since: Date.now()
          };

          // insert the stats
          storage.insert(globalStats, function(err, globalStats) {
            if (err) {
              reject(err);
            } else {
              self.globalStats = globalStats;
              resolve(globalStats);
            }
          });
        }
      });
    });
  }

  /**
   * @todo
  * removes a single application from the user's collected application history
  * @param {string} userID the discord id of the user
  * @param {string} serverID the server to check against
  * @param {string} appName the exact name of the application to purge
  * @return {promise} resolves on success, rejects on errors
  */
  purgeSingleApp(userID, serverID, appName) {
   return new Promise((resolve, reject) => {
     appName = appName.trim();
     this.storage.findOne({
       type: 'user-stats', userID: userID, serverID: serverID
     }, (err, userStats) => {
       if (err) {
         reject();
       } else {
         const index = userStats.game_history.indexOf(appName);
         if (index !== -1) {
          userStats.game_history.splice(index, 1);
          this.updateUserStats(userStats);
          resolve();
         } else {
          reject(`\`🎮 ${appName}\` wasn't in your application history. It is case sensitive.`);
         }
       }
     });
   });
 }

  /**
   * @todo
  * removes a user's collected application history
  * @param {string} userID the discord id of the user
  * @param {string} serverID the server to check against
  * @return {promise} resolves on success, rejects on errors
  */
  purgeAppHistory(userID, serverID) {
   return new Promise((resolve, reject) => {
     this.storage.findOne({
       type: 'user-stats', userID: userID, serverID: serverID
     }, (err, userStats) => {
       if (err) {
         reject();
       } else {
         userStats.game_history = [];
         this.updateUserStats(userStats);
         resolve();
       }
     });
   });
 }

  /**
   * gets a listing of the top active posters on the server
   * @param {string} serverID the server to check against
   * @param {number} count how many results to limit the search to
   * @param {number} age how many days ago users must have posted in
   * @return {promise} resolves with a collection of user stats
   */
 getTopPosters(serverID, count, age) {
   const stardate = moment();
   stardate.subtract(age, 'days');
   return new Promise((resolve, reject) => {
    this.storage.find({
      type: 'user-stats',
      serverID: serverID,
      last_spoke: {$gte: stardate.valueOf()},
      $not: {
        messages_sent: 0
      },
    }).sort({messages_sent:-1}).limit(count).exec((err, collection) => {
      if (err) {
        reject();
      } else if (collection) {
        collection = collection.map((x) => {
          x.name = x.last_known_as
                 || this.discord.getDisplayNameByID(serverID, x.userID);
          return x;
        });
        resolve(collection);
      }
    });
  });
 }

  /**
   * gets a user's saved stats from the database, creating them if empty
   * @param {string} userID the discord id of the user
   * @param {string} serverID the server to check against
   * @return {promise} resolves with the user's stats
   */
  getUserStats(userID, serverID) {
    return new Promise((resolve, reject) => {
      this.storage.findOne({
        type: 'user-stats', userID: userID, serverID: serverID
      }, (err, userStats) => {
        if (err) {
          reject();
        } else if (!userStats) {
          this.createUserStats(userID, serverID)
            .then(resolve)
            .catch(this.onError.bind(this));
        } else {
          resolve(userStats);
        }
      });
    });
  }

  /**
   * creates user stats for database with default values
   * @param {string} userID the discord id of the user
   * @param {string} serverID the server to check against
   * @return {promise} resolves with the user's stats
   */
  createUserStats(userID, serverID) {
    return new Promise((resolve, reject) => {
      this.storage.insert({
        type:          'user-stats',
        userID:        userID,
        serverID:      serverID,
        last_seen:     Date.now(),
        messages_sent: 0,
        links_sent:    0,
        mentions_sent: 0
      }, (err, userStats) => {
        if (err) {
          reject(err);
        } else {
          resolve(userStats);
        }
      });
    });
  }

  /**
   * updates user stats in the database after grabbing their last known name
   * @param {object} userStats the user stats object provided by this plugin
   */
  updateUserStats(userStats) {
    userStats.last_known_as = this.discord.getDisplayNameByID(
      userStats.serverID,
      userStats.userID
    );
    this.storage.update({
      type:'user-stats',
      userID: userStats.userID,
      serverID: userStats.serverID
    }, userStats);
  }

  /**
   * decorates a user stat object with updated message counters
   * @param {object} userStats the user stats object provided by this plugin
   * @param {string} message the message content provided by discord bot
   * @return {object} the user stats object with game history
   */
  decorateStatsWithCounters(userStats, message) {
    const now     = Date.now();

    // incriment user messages sent
    userStats.messages_sent++;
    userStats.last_spoke = now;

    // update links sent
    if(new RegExp('([a-zA-Z0-9]+://)([a-zA-Z0-9_]+:[a-zA-Z0-9_]+@)?([a-zA-Z0-9.-]+\\.[A-Za-z]{2,4})(:[0-9]+)?(/.*)?').test(message)) {
        userStats.links_sent++;
    }

    // calculate number of mentions
    const regexp = /(<\@[a-zA-Z0-9]+>)/g;
    let m;
    let matches = [];
    do {
        m = regexp.exec(message);
        if (m) {
            matches.push(m[1]);
        }
    } while(m);
    userStats.mentions_sent = userStats.mentions_sent + matches.length;

    return userStats;
  }

  /**
   * updates a user stat object with online status and last seen
   * @param {object} userStats the user stats object provided by this plugin
   * @param {string} status the status text provided by discord bot
   * @return {object} the user stats object with game history
   */
  decorateStatsWithActivity(userStats, status) {
    const now    = Date.now();

    if (userStats.last_status !== status) {
      userStats.last_status_time = now;
    }
    userStats.last_seen   = now;
    userStats.last_status = status;
    return userStats;
  }

  /**
   * updates a user stat object with game history
   * @param {object} userStats the user stats object provided by this plugin
   * @param {string} currentGame the current game entry provided by discord bot
   * @return {object} the user stats object with game history
   */
  decorateStatsWithGameHistory(userStats, currentGame) {
    if (currentGame) {
      currentGame = currentGame.replace(/\`/g, '\'');
    }
    userStats.game = currentGame || null;

    if (!currentGame) {
      // no games to add
      return userStats;
    }

    userStats.game_history = userStats.game_history || [];
    let gameAlreadyListed  = userStats.game_history.includes(currentGame);

    if (!gameAlreadyListed) {
      // new non-stream game detected, ensure we only store the last n games
      userStats.game_history.push(currentGame);

      const maxRemembered = this._settings.gameHistorySize || 10;
      if (userStats.game_history.length > maxRemembered) {
        const difference = userStats.game_history.length
                        - maxRemembered;
        userStats.game_history.splice(0, difference);
      }
    } else if (currentGame && gameAlreadyListed) {
      // game is already in the list, so move it to the top!
      const fIndex = userStats.game_history.indexOf(currentGame);
      userStats.game_history.splice(fIndex, 1);
      userStats.game_history.push(currentGame);
    }

    if (userStats.last_game !== currentGame) {
      userStats.last_game      = currentGame;
      userStats.last_game_time = Date.now();
    }

    return userStats;
  }

  /**
   * @param {string} serverID the server id currently in use
   * @param {number} messageCount the number of messages sent by the user
   * @return {object} the rank object which contains useful rank details
   */
  getRankFromMessageCount(serverID, messageCount) {
    if (
      this._settings.levelUp
      && this._settings.levelUp[serverID]
      && this._settings.levelUp[serverID].ranks
    ) {
      let   theRank    = null;
      const rankLength = this._settings.levelUp[serverID].ranks.length;

      for (let i = rankLength-1; i >= 0; --i) {
        if (messageCount >= this._settings.levelUp[serverID].ranks[i].postReq) {
          theRank = this._settings.levelUp[serverID].ranks[i];
          theRank.numeric = i+1;
          theRank.total   = rankLength;
          break;
        }
      }

      return theRank;
    } else {
      return false;
    }
  };

  /**
   * decorates a user stat object with updated message counters
   * @param {object} userStats the user stats object provided by this plugin
   * @param {string} channelID the channel id provided by discord bot
   * @return {promise} resolves with userStats when completed
   */
  handleMessagingRanks(userStats, channelID) {
    if (
      !this._settings.levelUp
      || !this._settings.levelUp[userStats.serverID]
      || !this._settings.levelUp[userStats.serverID].reportToChannel
    ) {
      return Promise.resolve(userStats);
    }

    const rank = this.getRankFromMessageCount(
      userStats.serverID,
      userStats.messages_sent
    );

    if (
      (!userStats.announcedRankAt || userStats.announcedRankAt !== rank.postReq)
      && !this.bot.users.cache.get(userStats.userID).bot
    ) {
      // announce the rank and save the value of announced rank to database
      if (rank && rank.message) {
        const levelUpMessage = ':up: ' + rank.message
                .replace(/\%rank/g, `\`${rank.rank}\``)
                .replace(/\%user/g, `<@${userStats.userID}>`);

        userStats.announcedRankAt = rank.postReq;

        const bundle = {
          channelID: channelID,
          response: {
            to: this._settings.levelUp[userStats.serverID].reportToChannel,
            message: levelUpMessage
          }
        };

        this.discord.respond(bundle);
      }
    }

    return Promise.resolve(userStats);
  }

  /**
   * error handler
   * @param {string} e the error message
   */
  onError(e) {
    this.warn(e);
  }
};

module.exports = Plugin;
