const BasicPlugin = use('classes/BasicPlugin');

/**
 * @alias Plugin.discordTips
 * @extends _.BasicPlugin
 */
class Plugin extends BasicPlugin {
  /**
   * runs the plugin code when called
   */
  init() {
    Promise.all([
      this._auriga.loadService('discord')
    ]).then((requirements) => {
      this.discord     = requirements[0];
      this.threshold = this._settings.threshold || 10;
      this.messageCounts = {};
      this.watchForHumanMessages();
      this.register();
      this._auriga.registerTask(
        'discord-tips',
        (this._settings.interval || 30),
        this.displayTip.bind(this)
      );
    });
  }

  /**
   * displays a registered tip
   */
  displayTip() {
    Object.keys(this._settings.channels).forEach(channel => {
      // check for message counts
      if (
        !this.messageCounts[channel] ||
        this.threshold > this.messageCounts[channel]
      ) {
        return;
      }

      // prepare message display
      let tips = this._settings.channels[channel].tips;
      if (tips && tips.length > 0) {
        let theTip   = tips[Math.floor(Math.random()*tips.length)];
        let response = this.prepareResponse(theTip, channel);
        this.messageCounts[channel] = 0;
        this.discord.respond(response);
      }
    });
  }

  /**
   * Count messages
   */
  watchForHumanMessages() {
    this.discord.bot.on('message', msg => {
      let watchedChannel = this._settings.channels[msg.channel.id];
      if (watchedChannel) {
        // watched channel, so lets save the author id for later referencing
        this.messageCounts[msg.channel.id] =
          this.messageCounts[msg.channel.id] || 0;

        if (msg.author.id !== this.discord.bot.user.id) {
          this.messageCounts[msg.channel.id]++;
        }
      }
    });
  }

  /**
   * prepares message strings with replacements
   * @param {string} str the message string to run replacements on
   * @param {string} channelID the channel ID to use
   * @return {string} the transformed string
   */
  prepareResponse(str, channelID) {
    str = str.replace(/\%p/g, `${this._auriga.config.prefix}`);
    let response = {
      channelID: channelID,
      response: {
        to: channelID,
        embed: {
          type: 'rich',
          title: this._settings.channels[channelID].title || 'Did you know?',
          description: this._settings.channels[channelID].description || '',
          color: 0x252525,
          fields: [{
            name : 'Tip',
            value: `${str}`
          }]
        }
      }
    };
    return response;
  }
}

module.exports = Plugin;
