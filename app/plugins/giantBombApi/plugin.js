const BasicPlugin  = use('classes/BasicPlugin');
const request      = require('request');

/**
 * @alias Plugin.giantBombApi
 * @extends _.BasicPlugin
 */
class Plugin extends BasicPlugin {
  /**
   * fetches game information from giantbomb servers
   * @param {string} query the search terms
   * @return {promise} resolves on success, rejects on errors
   */
  searchGames(query) {
    return new Promise((resolve, reject) => {
      const reqOptions = {
        url: `http://api.giantbomb.com/api/search/?format=json&api_key=${this._settings.api_key}&limit=10&resource=game&query=${encodeURIComponent(query)}`,
        headers: {
          'User-Agent': 'FortisBot'
        }
      };

      request(reqOptions, (error, response, body) => {
        if (!response.body || error) {
          reject('Couldn\'t find the game you were searching for');
        } else {
          const data = JSON.parse(response.body);
          if (data.results.length === 0) {
            reject('Couldn\'t find the game you were searching for');
          } else {
            resolve(data.results);
          }
        }
      });
    });
  };
};

module.exports = Plugin;
