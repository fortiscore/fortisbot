const BasicPlugin = use('classes/BasicPlugin');
const path        = require('path');
const NEDB        = require('nedb');

/**
 * @alias Plugin.streamRegistry
 * @extends _.BasicPlugin
 */
class Plugin extends BasicPlugin {
  /**
   * runs the plugin code when called
   */
  init() {
    Promise.all([
      this._auriga.loadService('twitch'),
      this._auriga.loadService('discord')
    ]).then((requirements) => {
      this.twitch  = requirements[0];
      this.discord = requirements[1];

      // set the storage
      this.storage = this._auriga.storage;

      // initiate the scheduled task
      this._auriga.registerTask('twitch-watch', 5, () => {
        this.update().then(() => {
          // be silent
        }).catch(this.warn);
      });

      this.register();
    });
  }

  /**
   * adds channels to the registry
   * @param {array} channels the channels to add to the registry
   * @return {promise} resolves on success, rejects on errors
   */
  addChannels(channels) {
    return new Promise((resolve, reject) => {
      channels.forEach((channel) => {
        let usingBot = false;
        if (channel.includes('+bot')) {
          usingBot = true;
          channel  = channel.replace(/\+bot/, '');
        }
        let displayName = channel.toLowerCase().trim();
        this.storage.update(
          {
            'type': 'stream-registry',
            'name': displayName
          },
          {
            'type': 'stream-registry',
            'name': displayName,
            'usingBot': usingBot
          },
          {'upsert':true},
          (err, numReplaced, upsert) => {
            if (err) {
              reject(err);
            } else {
              resolve();
            }
          }
        );
      });
    });
  }

  /**
   * removes channels from the registry
   * @param {array} channels the channels to remove from the registry
   * @return {promise} resolves on success, rejects on errors
   */
  removeChannels(channels) {
    return new Promise((resolve, reject) => {
      channels.forEach((channel) => {
        let displayName = channel.toLowerCase().trim();
        this.twitch.attemptChannelPart(displayName);
        this.storage.findOne(
          {
            'type': 'stream-registry',
            'name': displayName
          },
          (err, found) => {
            if (found) {
              this.storage.remove(
                {
                  'type': 'stream-registry',
                  'name': displayName
                },
                (err, numRemoved) => {
                  resolve();
                }
              );
            }
          }
        );
      });
    });
  }

  /**
   * gets channels from storage
   * @return {promise} resolves on success, rejects on errors
   */
  getChannels() {
    return new Promise((resolve, reject) => {
      this.storage.find({'type':'stream-registry'}, (err, streams) => {
        if (err) {
          return reject(err);
        }

        return resolve(streams);
      });
    });
  }

  /**
   * fetches channels from the twitch api
   * @return {promise} resolves on success, rejects on errors
   */
  fetchChannels() {
    return new Promise((resolve, reject) => {
      this.storage.find({'type':'stream-registry'}, (err, streams) => {
        if (err) {
          reject(err);
        } else {
          const channels = streams.map(x => x.name);
          const options  = {
            first: 100,
            user_login: channels.join('&user_login=')
          };
          this.twitch.fetch('streams', options).then((results) => {
            let streams = results.data || [];
            let userlist = [];
            let gamelist = [];
            streams.forEach((entry) => {
              // get user data and game data for fetching
              userlist.push(entry.user_id);
              gamelist.push(entry.game_id);
            });

            if (userlist.length > 0) {
              // attach all user and game data to the result
              Promise.all([
                this.twitch.fetch('users', {
                  first: 100,
                  id: userlist.join('&id=')
                }),
                this.twitch.fetch('games', {
                  first: 100,
                  id: gamelist.join('&id=')
                }),
              ]).then((results) => {
                let users = results[0];
                let games = results[1];

                streams.forEach((entry) => {
                  const userEntry = users.data.filter((x) => {
                    return x.id == entry.user_id;
                  });
                  entry.user = userEntry[0];
                  const gameEntry = games.data.filter((x) => {
                    return x.id == entry.game_id;
                  });
                  entry.game = gameEntry[0];
                });
                resolve(streams);
              });
            } else {
              resolve([]);
            }
            // resolve(results.streams);
          }).catch(resolve);
        }
      });
    });
  }

  /**
   * saves a single stream record to storage
   * @param {object} stream a single stream result provided by twitch
   * @param {boolean} usingBot true if we're using a bot, false otherwise
   */
  saveStreamToStorage(stream, usingBot = false) {
    this.storage.update(
      {'type': 'stream-registry', 'name': stream.user.login},
      {
        'type':         'stream-registry',
        'name':         stream.user.login,
        'user_id':      stream.user_id,
        'game':         stream.game.name,
        'game_id':      stream.game.id,
        'game_art':     stream.game.box_art_url,
        'status':       stream.title,
        'display_name': stream.user.display_name,
        'logo':         stream.user.profile_image_url,
        'url':          `https://twitch.tv/${stream.user.login}`,
        'usingBot':     usingBot,
        'online':       true
      },
      {'upsert': true}
    );
  }

  /**
   * updates streams in storage that are now offline
   * @param {array} onlineChannels a list of registered channel names that are online
   */
  updateOfflineStreams(onlineChannels) {
    this.storage.find(
      {'type': 'stream-registry'},
      (err, saved) => {
        if (!err) {
          saved.forEach((stream) => {
            if (stream.usingBot && onlineChannels.includes(stream.name)) {
              this.twitch.attemptChannelJoin(stream.name);
            } else {
              this.twitch.attemptChannelPart(stream.name);
            }

            if (!onlineChannels.includes(stream.name)) {
              this.storage.update(
                {'type':'stream-registry', 'name':stream.name},
                {
                  'type': 'stream-registry',
                  'name': stream.name,
                  'online': false,
                  'usingBot': stream.usingBot
                },
                {upsert: true}
              );
            };
          });
        }
      }
    );
  }

  /**
   * synchronizes local storage to reflect current stream status
   * @param {array} streams a list of streams provided by twitch
   * @return {promise} resolves on success, rejects on errors
   */
  syncStorage(streams) {
    return new Promise((resolve, reject) => {
      let nowOnline = streams.map(x => x.user.login);
      this.updateOfflineStreams(nowOnline);

      streams.forEach((stream) => {
        this.storage.findOne(
          {'type': 'stream-registry', 'name':stream.user.login},
          (err, saved) => {
            if (err) {
              return reject('Failed to read storage');
            }

            let usingBot = saved.usingBot || false;
            this.saveStreamToStorage(stream, usingBot);

            if (!saved.online) {
              this.announceStream(stream);
            }

            resolve('Update is now being processed!');
          }
        );
      });
    });
  }

  /**
   * updates the status of all channels
   * @return {promise} resolves on success, rejects on errors
   */
  update() {
    return new Promise((resolve, reject) => {
      this.fetchChannels()
      .then(this.syncStorage.bind(this))
      .then(resolve)
      .catch(resolve);
    });
  }

  /**
   * announces a stream as online to configured channels
   * @param {object} stream the stream object received from twitch
   */
  announceStream(stream) {
    let embedObj = this.generateDiscordEmbed(stream);
    let channels = this._settings.announce_channels;
    Object.keys(channels).forEach((channelID) => {
      const message = this._settings.announce_channels[channelID]
      .replace(/\%channel/g, stream.user.display_name)
      .replace(/\%game/g, stream.game.name)
      .replace(/\%status/g, stream.title)
      .replace(/\%url/g, `https://twitch.tv/${stream.user.login}`);

      this.discord.respond({
        channelID: channelID,
        response: {
          to: channelID,
          message: message,
          embed: embedObj
        }
      });
    });
  }

  /**
   * generates a discord embed object from twitch stream data
   * @param {object} stream the stream object received from twitch
   * @return {object} the discord embed object
   */
  generateDiscordEmbed(stream) {
    return {
      type: 'rich',
      title: `${stream.user.display_name} is live on Twitch!`,
      url: `https://twitch.tv/${stream.user.login}`,
      description: stream.title,
      color: 0x4B367C,
      fields: [
        {
          name: 'Game',
          value: stream.game.name,
          inline: true
        },
        {
          name: 'Channel',
          value: `https://twitch.tv/${stream.user.login}`,
          inline: true
        }
      ],
      thumbnail: {
        url: stream.user.profile_image_url
      },
      image: {
        url: stream.user.offline_image_url
      },
      footer: {
        text: 'Provided by Twitch.tv',
        icon_url: 'https://puu.sh/viQ06/11aba95012.png'
      }
    };
  }
};

module.exports = Plugin;
