const BasicPlugin = use('classes/BasicPlugin');

/**
 * @alias Plugin.webhooksBitbucket
 * @extends _.BasicPlugin
 */
class Plugin extends BasicPlugin {
  /**
   * runs the plugin code when called
   */
  init() {
    Promise.all([
      this._auriga.loadService('discord'),
      this._auriga.loadService('webLocalServer')
    ]).then((requirements) => {
      this.discord = requirements[0];
      this.web     = requirements[1];
      this.createRoute();
      this.register();
    });
  }

  /**
   * creates the web route
   */
  createRoute() {
    this.web.addPostRoute('/api/bitbucket', (req, res) => {
      if (this.isRepositoryPushEvent(req)) {
        const channels = this.getReportingChannels(req);

        if (channels) {
          // OK
          res.status(200).send('OK');
          this.reportCommits(channels, req);
        } else {
          // we're not watching this repo. Send Forbidden Status Code
          res.status(403).send('Code repository not whitelisted or no changes were detected');
        }
      } else {
        // no push object found, maybe an unsupported event? Send Not Acceptable Status Code
        res.status(406).send('Not a bitbucket push event');
      }
    });
  }

  /**
   * gets a list of channels to report to for a given repository if there are
   * changes to report
   * @param {string} req the express request object
   * @return {array} a list of channels to report to
   */
  getReportingChannels(req) {
    const repoUrl         = req.body.repository.links.html.href;
    const repoChangeCount = req.body.push.changes.length;
    let channels          = false;

    if (repoChangeCount > 0) {
      this._settings.watch.some((repo) => {
        if (repo.url === repoUrl) {
          channels = repo.channels;
        }
      });
    }

    return channels;
  }

  /**
   * checks if the request is a repository push event
   * @param {object} req the express request object
   * @return {boolean} true if valid push event, false otherwise
   */
  isRepositoryPushEvent(req) {
    return req.body && req.body.push;
  }

  /**
   * reports new commits through discord
   * @param {array} channels the list of channels to report to
   * @param {object} req the express request object
   */
  reportCommits(channels, req) {
    let discord = this.discord;
    let bot     = this.discord.bot;

    req.body.push.changes[0].commits.reverse().forEach((commit) => {
      // dont announce remote tracking merges if we haven't enabled it
      if (
        commit.message.startsWith('Merge remote-tracking') &&
        !this._settings.showRemoteMerges
      ) {
        // do not announce
      } else {
        // announce
        channels.forEach((channelID) => {
          if (bot.channels.cache.get(channelID)) {
            const ownerName = req.body.repository.project
                            ? req.body.repository.project.name
                            : req.body.repository.owner.nickname;
            discord.respond({
              channelID: channelID,
              response: {
                to: channelID,
                message: '',
                embed: {
                  type: 'rich',
                  title: `${ownerName}/${req.body.repository.name} (${req.body.push.changes[0].new.type}/${req.body.push.changes[0].new.name})`,
                  description: `Commit [${commit.hash.substr(0, 7)}](${commit.links.html.href})\n\n${commit.message}`,
                  url: req.body.repository.links.html.href,
                  color: 0x205081,
                  thumbnail: {
                    url: 'https://i.imgur.com/meogPcA.png'
                  },
                  author: {
                    name: commit.author.user.username,
                    url: `https://bitbucket.org/${commit.author.user.username}`,
                    icon_url: commit.author.user.links.avatar.href
                  }
                }
              }
            });
          }
        });
      }
    });
  }
};

module.exports = Plugin;
