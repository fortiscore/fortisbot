const BasicPlugin = use('classes/BasicPlugin');

/**
 * @alias Plugin.webhooksTrello
 * @extends _.BasicPlugin
 */
class Plugin extends BasicPlugin {
  /**
   * runs the plugin code when called
   */
  init() {
    Promise.all([
      this._auriga.loadService('discord'),
      this._auriga.loadService('webLocalServer')
    ]).then((requirements) => {
      this.discord  = requirements[0];
      this.web      = requirements[1];
      this.webIcons = this.web.registerStatic(`${__dirname}/icons`, this);
      this.createRoute();
      this.register();
    });
  }

  /**
   * creates the web route
   */
  createRoute() {
    // create get route that returns 200 (needed for webhook)
    this.web.addGetRoute('/api/trello', (req, res) => {
      res.status(200).send();
    });

    // create post route
    this.web.addPostRoute('/api/trello', (req, res) => {
      res.status(200).send();
      let idModel  = false;
      let channels = false;

      if (this.isTrelloEvent(req)) {
        this._settings.watch.some((entry) => {
          if (entry.idModel === req.body.model.id) {
            idModel = entry.idModel;
            channels = entry.channels;
          }
        });

        if (idModel && channels) {
          let url         = `https://trello.com/c/${req.body.action.data.card.shortLink}/`;

          // setup author
          let avatar      = `https://trello-avatars.s3.amazonaws.com/${req.body.action.memberCreator.avatarHash}/50.png`;
          let author      = `${req.body.action.memberCreator.fullName} @ ${req.body.action.data.board.name}`;
          let profile     = `http://trello.com/${req.body.action.memberCreator.username}`;

          let data        = {};
          let title       = false;
          let description = false;

          switch(req.body.action.type) {
            case `updateCard`:
              if (req.body.action.data.card.closed) {
                // archived
                // title       = `${req.body.action.data.list.name} > ${req.body.action.data.card.name}`;
                // description = `> Archived this card`;

              } else if (req.body.action.data.listBefore) {
                // moved to another list
                data = {
                  board: req.body.action.data.board.name,
                  list:  req.body.action.data.listAfter.name,
                  card:  req.body.action.data.card.name
                };
                title       = `${data.card}`;
                description = `in \`${data.list}\`\n\n`
                            + `Moved this card from \`${req.body.action.data.listBefore.name}\`\nto \`${data.list}\``;

              } else if (req.body.action.data.old && typeof req.body.action.data.old.desc === 'string') {
                // changed description
                data = {
                  board:       req.body.action.data.board.name,
                  list:        req.body.action.data.list.name,
                  card:        req.body.action.data.card.name,
                  description: req.body.action.data.card.desc
                };
                title       = `${data.card}`;
                description = `in \`${data.list}\`\n\n`
                            + `✏ ${data.description}`;

              } else if (req.body.action.data.old.name) {
                // changed name
                // title       = `${req.body.action.data.list.name} > ${req.body.action.data.card.name}`;
                // description = `> Changed name from ${req.body.action.data.old.name}`;
              }
              break;

            case `createCard`:
              if (req.body.action.data.card.desc && req.body.action.data.card.desc !== '') {
                data = {
                  board:       req.body.action.data.board.name,
                  list:        req.body.action.data.list.name,
                  card:        req.body.action.data.card.name,
                  description: req.body.action.data.card.desc || ''
                };
                title       = `${data.card}`;
                description = `in \`${data.list}\`\n\n`
                            + `📬 New Card\n\n${data.description}`;
              }
              break;

            case `deleteCard`:
              // title       = `${req.body.action.data.list.name} > ${req.body.action.data.card.name}`;
              // description = `> Deleted this card`;
              break;

            case `commentCard`:
              data = {
                board:       req.body.action.data.board.name,
                list:        req.body.action.data.list.name,
                card:        req.body.action.data.card.name,
                comment:     req.body.action.data.text
              };
              title       = `${data.card}`;
              description = `in \`${data.list}\`\n\n`
                          + `💬 ${data.comment}`;
              break;

            case `updateComment`:
              // list isn't available in this event
              break;

            case `deleteComment`:
              break;

            case `addLabelToCard`:
              break;

            case `removeLabelFromCard`:
              break;

            default:
              break;
          }

          if (title && description) {
            channels.forEach((channelID) => {
              let embed = {
                channelID: channelID,
                response: {
                  to: channelID,
                  message: '',
                  embed: {
                    type: 'rich',
                    title: title,
                    description: description,
                    url: url,
                    color: 0xFF00B4,
                    author: {
                      name: author,
                      url: profile,
                      icon_url: avatar
                    }
                  }
                }
              };

              if (this.webIcons) {
                embed.response.embed.thumbnail = {
                  url: `${this.webIcons}/logo.png`
                };
              }

              this.discord.respond(embed);
            });
          }
        }
      } else {
        // no push object found, maybe an unsupported event? Send Not Acceptable Status Code
        res.status(406).send('Not a trello event');
      }
    });
  }

  /**
   * checks if the request is a trello event
   * @param {object} req the express request object
   * @return {boolean} true if valid push event, false otherwise
   */
  isTrelloEvent(req) {
    return req.body.model.id && req.body.action.data.card;
  }
};

module.exports = Plugin;
