const BasicService     = use('classes/BasicService');
const RateLimitedQueue = use('classes/RateLimitedQueue');
const Discord          = require('discord.js');

/**
 * @alias Service.discord
 * @extends _.BasicService
 */
class Service extends BasicService {
  /**
   * code to run on load
   */
  init() {
    this.outboxes = [];

    this.bot = new Discord.Client();

    this.bot.on('message', msg => {
      const bundle = {
        username: msg.author.username,
        userID: msg.author.id,
        channelID: msg.channel.id,
        serverID: (msg.member) ? msg.member.guild.id : null,
        isDM: msg.channel.type === 'dm',
        source: msg
      };

      this.dispatchMessage(msg.content, bundle);
    });

    this.bot.on('ready', () => {
      let account = `${this.bot.user.username}#${this.bot.user.discriminator}`;
      this.info(`Connected with account '${account}'.`);
      this.register();
    });

    this.bot.login(this._settings.token);
  }

  /**
   * checks if a message outbox exists by name
   * @param {string} name the name of the outbox to check
   * @return {boolean} true if outbox exists, false otherwise
   */
  outboxExists(name) {
    return (typeof this.outboxes[name] !== 'undefined');
  }

  /**
   * @todo handle callbacks?
   * creates a message outbox with the provided name
   * @param {string} name the name of the outbox
   * @return {promise} resolves with the outbox that is created
   */
  createOutbox(name) {
    return new Promise((resolve, reject) => {
      this.outboxes[name] = new RateLimitedQueue({
        units: 1,
        per: 1000,
        action: (unit) => {
          let channel = this.getSendableChannel(unit.to);
          if (unit.embed) {
            channel.send({content: unit.message, embed: unit.embed});
          } else {
            channel.send(unit.message);
          }
        }
      });

      resolve(this.outboxes[name]);
    });
  }

  /**
   * retrieves a message outbox, creating it if needed
   * @param {string} name the name of the outbox
   * @return {object} the outbox requested
   */
  getOutbox(name) {
    return new Promise((resolve, reject) => {
      if (this.outboxExists(name)) {
        resolve(this.outboxes[name]);
      } else {
        this.createOutbox(name).then((createdOutbox) => {
          resolve(createdOutbox);
        });
      }
    });
  }

  /**
   * authenticates a user against a keychain
   * @param {object} keychain an array of keychain entries to check against
   * @param {object} bundle an object of meta data initially sent by the service
   * @return {promise} resolves with a username if authenticated.
   *                   throws an error and rejects if not authenticated.
   */
  authenticate(keychain, bundle) {
    return new Promise((resolve, reject) => {
      const key = keychain.some((key) => {
        key = key.toLowerCase();
        if (key === 'everyone') {
          // bundle.source.channel.startTyping();
          resolve({
            name: bundle.username,
            key: key
          });
        } else if (key.startsWith('uid:')) {
          // verify against a user id
          const requestID = key.replace('uid:', '');
          const userID    = bundle.userID;

          if (requestID === userID) {
            // bundle.source.channel.startTyping();
            resolve({
              name: bundle.username,
              key: key
            });
          }
        } else if (key.startsWith('rid:')) {
          // verify against a role id (server specific)
          const roleID    = key.replace('rid:', '');
          const serverID  = bundle.serverID;
          let guild = this.bot.guilds.cache.get(bundle.serverID);
          if (!guild) {
            // probably a DM
            reject('Cannot process commands through DM');
          }
          let member = guild.members.cache.get(bundle.userID);
          let role   = member.roles.cache.get(roleID);
          if (serverID && role) {
            // bundle.source.channel.startTyping();
            resolve({
              name: bundle.username,
              key: key
            });
          }
        }
      });

      if (key) {
        // bundle.source.channel.startTyping();
        resolve({
          name: bundle.userstate['display-name'],
          key:'everyone'
        });
      } else {
        reject('Not Authorized');
      }
    });
  }

  /**
   * wraps input in a code block
   * @param {string} syntax the syntax identifier
   * @param {string} input string input
   * @return {string} string output
   */
  codeBlock(syntax = '', input) {
    return `\`\`\`${syntax}\n${input}\n\`\`\``;
  }

  /**
   * searches for and returns a user's id by providing
   * all or part of the user's nickname / username
   * @param  {string} serverID [the server's id]
   * @param  {string} search   [the search string]
   * @return {variable}        [returns the id if found, false otherwise]
   */
  getUserIDByNameSearch(serverID, search) {
    const bot   = this.bot;
    const guild = bot.guilds.cache.get(serverID);
    search      = search.toString().toLowerCase();

    let result  = guild.members.cache.find((x) => {
      return (
        (x.nickname && x.nickname.toLowerCase().indexOf(search) > -1) ||
        (x.user.username && x.user.username.toLowerCase().indexOf(search) > -1)
      );
    });
    return (result && result.id) ? result.id : false;
  };

  /**
   * gets a user's display name (nickname or username) by their id
   * @param {string} serverID the discord serverID
   * @param {string} userID the user's discord userID
   * @return {string} the user's display name
   */
  getDisplayNameByID(serverID, userID) {
    const bot   = this.bot;
    const guild = bot.guilds.cache.get(serverID);
    let result  = guild.members.cache.find((x) => {
      return x.id === userID;
    });

    if (!result) {
      result = {
        nickname: `uid:${userID}`,
        user: {
          username: `uid:${userID}`
        }
      };
    }
    return (result.nickname) ? result.nickname : result.user.username;
  }

  /**
   * gets a role name by providing its id
   * @param {string} serverID the server the role exists on
   * @param {string} roleID the id of the role
   * @return {string} the role name
   */
  getRoleNameByID(serverID, roleID) {
    const bot   = this.bot;
    const guild = bot.guilds.cache.get(serverID);
    let role    = guild.roles.find((x) => {
      return x.id === roleID;
    });

    if (role) {
      return `role:${role.name}`;
    } else {
      return `rid:${roleID}`;
    }
  }

  /**
   * translates a keychain into human readible format
   * @param {string} rawKeychain the permission string
   * @param {object} bundle the service bundle
   * @return {object} the human readable keychain
   */
  translateKeychain(rawKeychain, bundle) {
    const allowed  = ['discord'];

    // clone the keychain so that we aren't editing actual values, then filter
    let keychain = JSON.parse(JSON.stringify(rawKeychain));

    // replace permission values with human readable names
    Object.keys(keychain).forEach((key) => {
      if (allowed.includes(key)) {
        let set = keychain[key];
        set.forEach((value, index) => {
          switch(value.substring(0, 4)) {
            case 'uid:':
              set[index] = this.getDisplayNameByID(
                bundle.serverID,
                value.substring(4)
              );
              break;
            case 'rid:':
              set[index] = this.getRoleNameByID(
                bundle.serverID,
                value.substring(4)
              );
              break;
            default:
              break;
          }
        });
      }
    });

    // return the human-readable keychain
    return JSON.stringify(keychain);
  }

  /**
   * searches for and returns a user id by checking
   * against multiple patterns
   *  - <@username>
   *  - userid
   *  - name search
   * @param  {string} serverID   [the server id]
   * @param  {string} userString [the string to parse user information from]
   * @return {variable}          [user id string if found, false otherwise]
   */
  resolveUserID(serverID, userString) {
    userString = userString.toString();
    let userID = false;
    if (userString !== '') {
      // did we provide a mention?
      let regexp = /<\@!?([a-zA-Z0-9]+)>/g;
      let match = regexp.exec(userString);
      if (match) {
        userID = match[1];
      } else {
        // did we provide an id string?
        regexp = /(^[0-9]+$)/g;
        match = regexp.exec(userString);
        if (match) {
          userID = match[1];
        } else {
          // fallback to name search
          match = this.getUserIDByNameSearch(
            serverID,
            userString.toLowerCase()
          );
          if (match) {
            userID = match;
          }
        }
      }
    }
    return userID;
  };

  /**
   * gets either the channel or user object for sending a message
   * @param {string} id the id of either a user or channel
   * @return {object} the object with which you can use the send method on
   */
  getSendableChannel(id) {
    let channel = this.bot.channels.cache.get(id);
    if (!channel) {
      channel = this.bot.users.cache.get(id);
    }
    return channel;
  }

  /**
   * handle a response from a command
   * @param {object} bundle the original message bundle with command response
   *                        added
   */
  respond(bundle) {
    let channel = this.getSendableChannel(bundle.channelID);
    const isDM  = (channel.username || (channel.type && channel.type === 'dm')) ? true: false;

    const outboxName = (isDM)
                     ? 'DirectMessages'
                     : channel.guild.id;

    if (
      typeof bundle.response === 'object'
      && bundle.response.to
      && (bundle.response.message || bundle.response.embed)
    ) {
      this.getOutbox(outboxName).then((outbox) => {
        outbox.add(bundle.response);
      });
    } else if (typeof bundle.response === 'string') {
      const responseObj = {
        to: bundle.channelID,
        message: `<@${bundle.userID}>: ${bundle.response}`
      };

      this.getOutbox(outboxName).then((outbox) => {
        outbox.add(responseObj);
      });
    }
  }
};

module.exports = Service;
