const BasicService     = use('classes/BasicService');
const RateLimitedQueue = use('classes/RateLimitedQueue');
const request          = require('request');
const TMI              = require('tmi.js');

/**
 * @alias Service.twitch
 * @extends _.BasicService
 */
class Service extends BasicService {
  /**
   * runs code on launch
   */
  init() {
    // setup api
    this.baseUrl    = 'https://api.twitch.tv/helix';
    this.chatterUrl = 'https://tmi.twitch.tv/group/user/%CHANNEL%/chatters';

    // setup bot
    // eslint-disable-next-line new-cap
    this.bot = new TMI.client({
      options: {
        debug: false
      },
      connection: {
        cluster: 'aws',
        reconnect: true
      },
      identity: {
        username: this._settings.chatbot.username,
        password: this._settings.chatbot.password
      }
    });

    this.outbox = new RateLimitedQueue({
      units: 100,
      per: 30000,
      action: (unit) => {
        this.bot.say(unit.channel, unit.message);
      }
    });

    this.bot.on('connected', (tserver) => {
      this.info(`Connected with account '${this.bot.username}'.`);
      this.register();
    });

    this.bot.on('message', (channel, userstate, message, self) => {
      const bundle = {
        channel: channel,
        userstate: userstate
      };
      this.dispatchMessage(message, bundle);
    });

    this.bot.connect();
  }

  /**
   * attempts to join a channel
   * @param {string} channel the twitch channel to connect to
   */
  attemptChannelJoin(channel) {
    if (!this.bot.getChannels().includes(`#${channel}`)) {
      this.bot.join(channel).then((data) => {
        this.info(`Joined channel '${channel}'`);
        this.outbox.add({
          channel: channel,
          message: `Reporting for duty, ${channel}!`
        });
      }).catch((err) => {
        this.warn(`Error joining channel '${channel}': ${err}`);
      });
    }
  }

  /**
   * attempts to part a channel
   * @param {string} channel the twitch channel to connect to
   */
  attemptChannelPart(channel) {
    if (this.bot.getChannels().includes(`#${channel}`)) {
      this.bot.part(channel).then((data) => {
        // console.log('LEAVE SUCCESS', data);
      }).catch(this.warn);
    }
  }

  /**
   * checks if a user is a broadcaster
   * @param {object} userstate the userstate object provided by twitch
   * @return {boolean} returns true if the user is a broadcaster
   */
  userIsBroadcaster(userstate) {
    return (
      userstate.badges
      && userstate.badges.broadcaster === '1'
    );
  }

  /**
   * checks if a user is a moderator
   * @param {object} userstate the userstate object provided by twitch
   * @return {boolean} returns true if the user is a moderator
   */
  userIsModerator(userstate) {
    return userstate.mod;
  }

  /**
   * checks if a user is a moderator
   * @param {object} userstate the userstate object provided by twitch
   * @return {boolean} returns true if the user is a moderator
   */
  userIsSubscriber(userstate) {
    return userstate.subscriber;
  }

  /**
   * makes a request to the twitch api
   * @param {string} endpoint the api endpoint to request
   * @param {object} params a list of parameters to send to the endpoint
   * @return {promise} resolves on success, rejects on errors
   */
  fetch(endpoint, params={}) {
    const options = {
      url: `${this.baseUrl}/${endpoint}?${this.serialize(params)}`,
      headers: {
        'Client-ID': this._settings.api.client_id
      }
    };

    return new Promise((resolve, reject) => {
      request(options, (error, response, body) => {
        if (error) {
          reject(error);
        } else {
          resolve(JSON.parse(body));
        }
      });
    });
  }

  /**
   * returns a list of active chat viewers
   * @param {string} channel the twitch channel name to check
   * @return {promise} resolves on success, rejects on errors
   */
  getChatViewers(channel) {
    channel   = channel.replace('#', '');
    const url = this.chatterUrl.replace('%CHANNEL%', channel);
    return new Promise((resolve, reject) => {
      request(url, (error, response, body) => {
        if (error) {
          reject(error);
        } else {
          resolve(JSON.parse(body).chatters);
        }
      });
    });
  }

  /**
   * Serializes an object of parameters into a url parameter string
   * @param {object} obj an object containing url paramaeters
   * @return {string} the url parameter string
   */
  serialize(obj) {
    let str = [];
    for(let p in obj)
      if (obj.hasOwnProperty(p)) {
        str.push(p + '=' + obj[p]);
      }
    return str.join('&');
  }

  /**
   * translates a keychain into human readible format
   * @param {string} rawKeychain the permission string
   * @param {object} bundle the service bundle
   * @return {object} the human readable keychain
   */
  translateKeychain(rawKeychain, bundle) {
    return JSON.stringify(rawKeychain);
  }

  /**
   * authenticates a user against a keychain
   * @param {object} keychain an array of keychain entries to check against
   * @param {object} bundle an object of meta data initially sent by the service
   * @return {promise} resolves with a username if authenticated.
   *                   throws an error and rejects if not authenticated.
   */
  authenticate(keychain, bundle) {
    return new Promise((resolve, reject) => {
      let authorizedKey = false;
      keychain.some((key) => {
        key = key.toLowerCase();
        if (key === 'everyone') {
          // everyone key
          authorizedKey = key;
          return true;
        } else if (
          key === 'broadcaster'
          && this.userIsBroadcaster(bundle.userstate)
        ) {
          // user is a broadcaster
          authorizedKey = key;
          return true;
        } else if (
          key === 'moderator'
          && (
            this.userIsModerator(bundle.userstate)
            || this.userIsBroadcaster(bundle.userstate)
          )
        ) {
          // user is a broadcaster or moderator
          authorizedKey = key;
          return true;
        } else if (
          key === 'subscriber'
          && (
            this.userIsSubscriber(bundle.userstate)
            || this.userIsBroadcaster(bundle.userstate)
          )
        ) {
          // user is a broadcaster or subscriber
          authorizedKey = key;
          return true;
        } else if (
          key.toLowerCase().startsWith('una:')
        ) {
          // user matches the username provided
          const uname = key.toLowerCase().replace('una:', '');
          if (bundle.userstate.username === uname) {
            authorizedKey = key;
            return true;
          }
        }
      });

      if (authorizedKey) {
        resolve({
          name: bundle.userstate['display-name'],
          key: authorizedKey
        });
      } else {
        reject('Not Authorized');
      }
    });
  }

  /**
   * handle a response from a command
   * @param {object} bundle the original message bundle with command response
   *                        added
   */
  respond(bundle) {
    this.outbox.add({
      channel: bundle.channel,
      message: `${bundle.userstate['display-name']}: ${bundle.response}`
    });
    /*
    this.bot.sendMessage({
      to: bundle.channelID,
      message: `<@${bundle.userID}>: ${bundle.response}`
    });
    */
  }
};

module.exports = Service;

