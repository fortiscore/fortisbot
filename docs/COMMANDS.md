# Commands
Welcome, visitor! This is the command help file for the auriga.io bot framework! The following commands can be run by using the prefix defined on your server (example: `!8ball` to run the 8ball command if `!` is the prefix on your server).

---

## 8ball
Ask the magic 8-ball a question!

```
8ball Am I going to get a promotion today?
```

```
@Frosthaven: Cannot predict now
```

---

## anime

`Service:discord`

Look up information about anime via kitsune.io. You can also get a list of anime by using the `--list` tag (example: `anime megaman --list`). From list view you can select a specific entry (example: `anime megaman --2`)

```
anime megaman axess
```

```
MegaMan: Axess
Rockman.EXE Axess

Synopsis
Netto's father Yuuichirou Hikari has made a scientific breakthrough by introducing the "synchro chips". If an operator and his or her navi are in a special enviroment known as a "dimensional area", they can fuse together in the real world via a technique called "cross fusion"! Yuuichirou's first test subject, Misaki Gorou, attempts the process and sadly fails. Netto offers to try with Rockman, but his father forbids it. Cross Fusion puts enormous strain on the operator's health, and battling in the real world could mean death.
(Source: Official Site)
```

---

## attack

`Service:discord`

Perform an attack on anyone and anything. This is a playful command that can take plain text or mentions in an argument and turn it into an attack. Useful as an alternative to a raw mention if you want to get someone's attention! This command relies on an included strings.json file that can be expanded as needed.

```
attack @DevBot, @FortisBot, and the clouds
```

```
@Frosthaven: Mustering the power of a thousand rabbits, Frosthaven hops on 'DevBot, FortisBot, and the clouds' for 3605 damage.
```

---

## choose
Choose from a comma separated list of options.

```
choose red, blue, green, yellow
```

```
@Frosthaven: blue
```

---

## coin
Flip a coin, heads or tails!

```
coin
```

```
@Frosthaven: coin landed on tails
```

---

## define

`Service:discord`

Get the definition of a word, powered by Merriam Webster.

```
define kleptocracy
```

```
: government by those who seek chiefly status and personal gain at the expense of the governed

: a particular government of this kind
```

---

## dice
Roll the dice. More advanced dice rolls are also supported (`dice 2d6`, `dice 2d6+10`, `dice 2d6-10`, etc).
```
dice 2d12+4
```

```
@Frosthaven: rolled 12 using 2d12+4 (1, 7)
```

---

## editmsg

`Service:discord`

Allows the user to edit bot messages using the message right click > copy message link option.

```
editmsg https://discordapp.com/channels/141907633077616640/761904272992239647/762062580100628513 new message!
```

---

## eval
Runs raw javascript.
WARNING: DO NOT GIVE THIS PERMISSIONS TO ANYONE YOU DONT TRUST. USERS CAN DESTROY YOUR PC WITH ACCESS.
```
eval
```

---

## game

`Plugin:giantBombApi` `Service:discord`

Look up information about video games via Giant Bomb. You can also get a list of games by using the `--list` tag (example: `game megaman --list`). From list view you can select a specific entry (example: `game megaman --2`)

```
game megaman 9
```

```
Mega Man 9
Synopsis
Mega Man returns to his roots in this deliberately retro side-scrolling platformer that simulates the style and simplicity of the first six Mega Man games.
```

---

## groups

`Service:discord`

Gets information about groups (special roles) that users can join and leave at will.

```
groups
```

```
@Frosthaven: Use `group join groupname` to join a group and `group leave groupname` to leave:

⚑ Fallout 76 ⚑ Minecraft
```

---

## groupmod

`Service:discord`

Manage groups for users to opt into.

### groupmod add

```
groupmod add Fallout 76
```

```
@Frosthaven: Added group ⚑ Fallout 76
```

### groupmod remove

```
groupmod remove Fallout 76
```

```
@Frosthaven: Removed group ⚑ Fallout 76
```

---

## help
Displays help documentation

```
help
```

```
@Frosthaven: Command Documentation: https://bitbucket.org/Frosthaven/auriga.io/src/master/COMMANDS.md
```

---

## image

`Plugin:googleApi` `Service:discord`

Finds a random image matching your search query using google. Relies on the server having auto-embedding of links to display images inline.

```
image sleepy kitty
```

```
@Frosthaven: http://sequart.org/images/sleepy-kitty.jpg
```

---

## keychain

`Service:discord`

Displays keychain permission data for a given command

```
keychain dice
```

```
dice
{"discord":["everyone"],"twitch":["everyone"]}
```

---

## math

Calculates mathematical expressions using [mathjs](http://mathjs.org/docs/index.html).

```
math 3*9
```

```
@Frosthaven: 27
```

---

## mem

`Service:discord`

Gives detailed information about the bot framework (memory and disk usage, installed features, etc)

---

## modstrike

`Service:discord`

Moderation strike management for tracking user infractions. Infractions are handled per-server, so be sure to use the command in the server where the infraction took place (a private channel, perhaps).

### modstrike add

Adds a strike against a user with a provided userid and reason. You may also use the user's name or partial name to find them instead of their id (be careful!).

```
modstrike add 141906786750955521 No political discussion is allowed on our server. Please review the rules in the #welcome channel!
```

```
@Frosthaven: strike 1 has been added and `Frosthaven` has been privately messaged the reason.
```

### modstrike find

Finds strikes placed against a user. You may also use the user's name or partial name to find them instead of their id (be careful!).

```
modstrike find 141906786750955521
```

```
@Frosthaven: `Frosthaven` has 1 strike(s). Showing up to 10 most recent:

kZtb4oSsJLTcSpGf: a few seconds ago : No political discussion is allowed on our server. Please review the rules in the #welcome channel!
```

### modstrike remove

Removes a moderation strike by its id.

```
modstrike remove kZtb4oSsJLTcSpGf
```

```
@Frosthaven: strike deleted
```

### modstrike clear

Removes all strikes placed against a user by their id. You may also use the user's name or partial name to find them instead of their id (be careful!).

```
modstrike clear 141906786750955521
```

```
@Frosthaven: cleared all strikes against `Frosthaven`
```

---

## quote

Pulls a random quote from the quote database.
```
quote
```

```
@Frosthaven: "he topped my bottom when i should of jungled his nexus" -Kurosen
```

---

## quotemod

`Command:quote`

Assorted quote moderation commands to manage your quote database.

### quotemod find
Finds quotes based on search terms and also shows their saved ids.

```
quotemod find topped my bottom
```

```
@Frosthaven: found 1 quote(s):
hVJQkiTCUpTUAO1a: "he topped my bottom when i should of jungled his nexus" -Kurosen
```

### quotemod add
Adds a quote to the database.

```
quotemod add "Making command readme files is fun" -Frosthaven
```

```
@Frosthaven: added quote!
```

### quotemod remove
Deletes a quote with the provided id.
```
quotemod remove hVJQkiTCUpTUAO1a
```

### quotemod update
Updates a quote in the database

```
quotemod update hVJQkiTCUpTUAO1a "I replaced a quote with this one!" -Frosthaven
```

```
@Frosthaven: updated quote
```

---

## ranks

`Plugin:discordStats` `Service:discord`

Lists messaging ranks on the discord server.

```
ranks
```

```
These are ranks earned by chatting on this server

The Introduced Newbie (1+)
I'm Around (25+)
The Familiar Face (100+)
The Regular (200+)
The Conversationalist (500+)
The Avatar Of Chat (2K+)
Dragonkin (5K+)
Dragon Overlord (10K+)
Ultra Dragon Overlord (25K+)
Time-Lost Lord of Dragons (40K+)
```

---

## roles

`Service:discord`

Lists user roles along with their id on the discord server.

```
roles
```

```
#SERVER ROLES
[Fortis Admin](141908162637856768)
[Twitch Subscriber](338087521756119040)
[everyone](141907633077616640)
```
---

## room

`Service:discord`

Assorted room commands to create and manage your very own voice channel rooms in discord.

### room
Creates a public voice channel room with the name provided on the discord server. Rooms will automatically be removed if they are unoccupied. If no name is provided, your discord name will be used.

```
room PUBG open games
```

```
@Frosthaven: Public voice channel `PUBG open games` created! This room will automatically delete when empty if you do not right click to delete it manually.
```

### room private
Creates a private voice channel room with the name provided on the discord server. You will need to invite other users before they can join. If no name is provided, your discord name will be used.

```
room private PUBG streaming
```

```
@Frosthaven: Private voice channel `PUBG streaming` created! Use \`!room invite @user @user\` to invite people, roles, or otherwise!  This room will automatically delete when empty if you do not right click to delete it manually.
```

### room invite
Invites users to your private voice room created with the `room` command. This will allow them to join your voice room.
```
room invite @HiddenSpikeTrap @TheUtilityMan @Kurosen
```

```
@Frosthaven: All Invites Sent
```

### room x
Manually deletes any voice rooms you created on the server.
```
room x
```

```
@Frosthaven: room succesfully deleted
```

---

## screenshare

`Service:discord`

After joining a voice lobby (whether predefined or created), you can use this command to open a screen sharing session within the voice lobby. Users with access to the lobby can click the link to show the screensharing widget.

```
screenshare
```

```
Frosthaven has invited everyone to a screenshare session!
Join 🔈 voice-lobby and then click here to participate!
```


## signup

`Service:discord`

### signup create

Creates a signup notification group and message in either the current channel or one from settings if provided.

```
signup create among us
```

```
@Frosthaven Signup group added
```

### signup remove

Removes a signup notification group and associated messages

```
signup remove among us
```

```
@Frosthaven Signup group deleted if it existed
```

## source

`Service:discord`

```
source
```

```
Frosthaven/auriga.io (branch/master)
auriga.io is a platform agnostic community bot that provides users with commands that they can run. Initially built for support with twitch chat and discord chat, auriga.io can take you as far as you want it to with its extendable class-inheritance based modular design and tight permission controls.
```

---

## stats

`Plugin:discordStats` `Service:discord`

Displays your discord user stats. You can also specify another user by fuzzy search `stats frost`, id `stats 141906786750955521`, or mention `stats @frosthaven`. Stats include historical join and creation dates, status, game history, messaging ranks, and more.

### stats --purge

```
stats --purge
```

```
@Frosthaven: Your application history has been reset!
```

### stats --purge SpecificAppName

Removes a specific application from your application history (case sensitive)

```
stats --purge Sublime Code Editor
```

```
@Frosthaven: Removed `🎮 Sublime Code Editor` from your application history!
```

### stats --leaderboard

Shows a leaderboard of the top posters that have posted within the last 30 days

```
stats --leaderboard
```

```
@Frosthaven: Here are the top 9 users that have posted within the last 30 days:
1. Frosthaven (67402)
2. Trey (33732)
3. FortisBot (16546)
4. JayDMu (ButtsSenpai) (9426)
5. LiftsLikeGaston (3303)
6. DevBot (2056)
7. SirMrCl3an (124)
8. Dannythetwo (4)
9. Dragon Mom Asura (3)
```

---

## streamlotto

`Service:twitch` `Plugin:twitchApi`

This command will randomly select a user who is in the channel's chat room. You may also include staff members (broadcaster, moderators, etc) by adding `+staff` to the command

```
streamlotto
```
```
Frosthaven: The user miryxm was selected!
```

```
streamlotto +staff
```
```
Frosthaven: The user frosthaven was selected!
```

---

## streamreg

`Plugin:twitchApi` `Service:discord`

Various commands to enable and manage a twitch stream registry that auto-posts streams

### streamreg list
Lists all channels saved to the stream registry along with their online status

```
streamreg list
```

```
@Frosthaven: [x] fortiscore [x] largoatdibbz
```

### streamreg add
Adds channels to the stream registry (space separated), and then posts an updated list.

```
streamreg add frosthaven hiddenspiketrap
```

```
@Frosthaven: [x] fortiscore [x] frosthaven [x] hiddenspiketrap [x] largoatdibbz
```

You can also append +bot to the username (don't add a space between the name and +bot), in order to have the twitch bot join the channel when it is online
```
streamreg add frosthaven+bot hiddenspiketrap
```

```
@Frosthaven: [x] fortiscore [x] frosthaven (+bot) [x] hiddenspiketrap [x] largoatdibbz
```

### streamreg remove
Removes channels from the stream registry (space separated), and then posts an updated list.

```
streamreg remove frosthaven fortiscore
```

```
@Frosthaven: [x] hiddenspiketrap [x] largoatdibbz
```

### streamreg update
Forces an immediate refresh of stream status from the Twitch API. Please note that the Twitch API has a ~5 minute update interval internally, so new streams will not be listed immediately as online.

```
streamreg update
```

```
@Frosthaven: Update is now being processed!
```

---

## streams

`Command:streamreg`

Returns a list of currently online streams that are saved to the stream registry (from the `streamreg` command).

```
streams
```

```
@Frosthaven: The following streams are currently online:

fortiscore is playing Xenogears: Xenogears with HiddenSpikeTrap & TheUtilityMan!
https://www.twitch.tv/fortiscoregaming

frosthaven is playing VSCode: Random classes!
https://www.twitch.tv/frosthaven
```


---

## streamvote

`Service:twitch` `Plugin:streamVoting`

Allows users to create a voting poll for twitch chat participants that lasts for 2 minutes. Users can respond to the poll using `number` or `#number`. In the event of a tie, the winner is decided at random. It is recommended to only allow broadcasters and moderators to run this command. Command format is shown in the example below:

```
streamvote Which direction should I go? *north *south *east *west
```

```
2m to vote: Which direction should I go? (1) north (2) south (3) east (4) wes
```

```
#3
```

```
The poll has ended! "east" wins with 1 vote(s)!
```

---

## video

`Plugin:googleApi` `Service:discord`

Finds a random youtube video matching your search query using google. Relies on the server having auto-embedding of links to display images inline.

```
video ghost town adam lambert
```

```
@Frosthaven: https://www.youtube.com/watch?v=lDoXekDxHIU
```

---

## weather

`Plugin:darkSkyWeatherApi` `Service:discord`

Does a weather lookup with city, state, zipcode, etc using the darksky api. You may also append your search query with `--c` to get results in celcius instead of fahrenheit.

```
weather 32211
```

```
Weather for Jacksonville, FL 32211, USA

Current
30°C
70% Humidity
0% Precipitation
Mostly Cloudy

[...continued]
```
