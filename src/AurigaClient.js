require('events').EventEmitter.prototype._maxListeners = 50;

const path         = require('path');
const EventEmitter = require('events').EventEmitter;
const NEDB         = require('nedb');
const moment      = require('moment');
const chalk        = require('chalk');
const AutoLoader   = require('./AutoLoader');

/**
 * global method for requiring from the src folder level
 * @param {string} fPath the file path to require()
 * @return {*} the results of require()
 */
global.use = (fPath) => {
  return require('./' + fPath);
};

/**
 * Class representing an AurigaClient
 * @alias _.AurigaClient
 * @extends EventEmitter
 */
class AurigaClient extends EventEmitter {

  /**
   * Creates an AurigaClient
   * @param {object} config The config object used to build the client
   * @param {string} config.prefix the prefix expected for command messages
   * @param {string} config.storage path to the desired storage database file
   * @param {string} config.serviceDir the directory to load services from
   * @param {string} config.pluginDir the directory to load plugins from
   * @param {string} config.commandDir the directory to load commands from
   * @todo dont make autoloader require js filenames. instead just verify there is a .json and a .js file (maybe?)
   */
  constructor(config) {
    super();

    // set the default configuration and attach the helper library
    const rootDir = process.cwd();
    this.config = config;

    // set the storage
    this.storage = new NEDB({
      filename: path.normalize(`${rootDir}/${config.storage}`),
      autoload: true
    });
    // auto-compact every 10 minutes
    this.storage.persistence.setAutocompactionInterval(60000*10);

    // prepare a load chain
    let loadChain = {
      'commands': {
        fileDir: path.normalize(`${rootDir}/${config.commandDir}`),
        fileName: 'command'
      },
      'services': {
        fileDir: path.normalize(`${rootDir}/${config.serviceDir}`),
        fileName: 'service'
      },
      'plugins': {
        fileDir: path.normalize(`${rootDir}/${config.pluginDir}`),
        fileName: 'plugin'
      }
    };

    // autoload classes and initialize them based on the above chainloader
    let totalLoaders    = Object.keys(loadChain).length;
    let finishedLoaders = 0;
    Object.keys(loadChain).forEach((loader) => {
      this[loader] = {};
      new AutoLoader({
        fileDir: loadChain[loader].fileDir,
        fileName: loadChain[loader].fileName,
        auriga: this
      }).load().then((collection) => {
        Object.keys(collection).forEach((key) => {
          this.log(`Loading ${loader} => ${key}`);
          if (this.isClass(collection[key].Class)) {
            this[loader][key] = new collection[key].Class(
              this,
              key,
              collection[key]._settings
            );
            this[loader][key].init();
          }
        });
        finishedLoaders++;
        if (finishedLoaders === totalLoaders) {
          this.emit('ready');
        }
      }).catch((err) => {
        this.error(`[${loader} loader]: ${err}`);
      });
    });
  }

  /**
   * handles a message dispatched by a service
   * @param {string} serviceName the service name that triggered the dispatch
   * @param {object} message the message object
   * @param {object} bundle a bundle that will be sent back to the service after
   *                        command authorization is completed successfully.
   */
  dispatchedMessage(serviceName, message, bundle={}) {
    // check if it is a valid command
    if (this.isValidCommandFormat(message)) {
      // split the message into command and string format
      const parts = this.extractCommandFromString(message);
      parts.cmd = parts.cmd.replace(this.config.prefix, '');

      if (this.commandIsLoaded(parts.cmd)) {
        const command  = this.commands[parts.cmd];
        if (!command._registered) {
          this.warn(`Command '${command._name}' was called but is not registered`);
          return;
        }
        const settings = this.commands[parts.cmd]._settings;
        if (
          typeof settings.keychain === 'object'
          && this.isArray(settings.keychain[serviceName])
        ) {
          // run the authenticator from the requested service
          const service  = this.services[serviceName];
          const keychain = settings.keychain[serviceName];
          service.authenticate(keychain, bundle).then((pair) => {
            // console.log(`authenticated ${serviceName}/${pair.name} for ${parts.cmd} with key: ${pair.key}`);

            // run the command and send the response back to the service
            command.action(parts.str, bundle, service).then((response) => {
              if (typeof response === 'number') {
                response = response.toString();
              }
              bundle.response = response;
              service.respond(bundle);
            }).catch((error) => {
              this.warn(error);
            });

          }).catch((error) => {
            this.warn(error);
          });
        };
      }
    }
  }

  /**
   * takes a string and converts it into a command and accompanying text
   * @param {string} str the string to convert
   * @return {object} an object containing the cmd and str
   */
  extractCommandFromString(str) {
    const args = str.split(' ');
    const cmd = args.shift();
    return {
      cmd: cmd,
      str: args.join(' ').trim()
    };
  }

  /**
   * checks if a command is loaded into the platform
   * @param {string} cmd checks if a requested command exists in the platform
   * @return {boolean} true if the command exists
   */
   commandIsLoaded(cmd) {
     return (
       this.commands[cmd]
       && typeof this.commands[cmd].action === 'function'
     ) || false;
   }

  /**
   * checks if a message meets the criteria to to be considered a command message
   * @param {string} message the message to check
   * @return {boolean} true if the message is in comamand format
   */
  isValidCommandFormat(message) {
    return (
      message.startsWith(this.config.prefix)
      && message.length > (this.config.prefix.length)
    );
  }

  /**
   * determines if an object is an array
   * @param {object} obj the object to check
   * @return {boolean} true if the object is an array
   */
  isArray(obj) {
    return (Object.prototype.toString.call(obj) === '[object Array]' );
  }

  /**
   * determines if a function is an ecmascript class
   * @param {function} fn the function to check if is class
   * @return {boolean} true if it is a class, false if it is not
   */
  isClass(fn) {
    return typeof fn === 'function' && /^\s*class\s+/.test(fn.toString());
  }

  /**
   * registers a service with auriga
   * @param {name} name the name of the service
   * @param {*} service the service contents
   */
  registerService(name, service) {
    this.services[name]._ready = true;
    this.emit('service-ready', name, service);
  }

  /**
   * registers a plugin with auriga
   * @param {name} name the name of the plugin
   * @param {*} plugin the plugin contents
   */
  registerPlugin(name, plugin) {
    this.plugins[name]._ready = true;
    this.emit('plugin-ready', name, plugin);
  }

  /**
   * registers a command with auriga
   * @param {name} name the name of the command
   * @param {*} command the command contents
   */
  registerCommand(name, command) {
    this.commands[name]._ready = true;
    this.emit('command-ready', name, command);
  }

  /**
   * provides a service
   * @param {string} name the name of the service to provide
   * @return {promise} resolves when the service is available
   */
  loadService(name) {
    return new Promise((resolve, reject) => {
      if (this.services && this.services[name] && this.services[name]._ready) {
        resolve(this.services[name]);
      } else {
        this.on('service-ready', (serviceName, service) => {
          if (serviceName === name) {
            resolve(service);
          }
        });
      }
    });
  }

  /**
   * provides a plugin
   * @param {string} name the name of the plugin to provide
   * @return {promise} resolves when the plugin is available
   */
  loadPlugin(name) {
    return new Promise((resolve, reject) => {
      if (this.plugins && this.plugins[name] && this.plugins[name]._ready) {
        resolve(this.plugins[name]);
      } else {
        this.on('plugin-ready', (pluginName, plugin) => {
          if (pluginName === name) {
            resolve(plugin);
          }
        });
      }
    });
  }

  /**
   * provides a command
   * @param {string} name the name of the command to provide
   * @return {promise} resolves when the command is available
   */
  loadCommand(name) {
    return new Promise((resolve, reject) => {
      if (this.commands && this.commands[name] && this.commands[name]._ready) {
        resolve(this.commands[name]);
      } else {
        this.on('command-ready', (commandName, command) => {
          if (commandName === name) {
            resolve(command);
          }
        });
      }
    });
  }

  /**
   * registers a looping task. this will run the task once when it's registered
   * and again every time the interval is reached.
   * @param  {string}   name     [the desired task name (must be unique)]
   * @param  {number}   minutes  [the run interval in minutes]
   * @param  {Function} callback [function for task actions]
   */
  registerTask(name, minutes, callback) {
    const timer = parseInt(minutes) * 60000;

    this.tasks = this.tasks || {};

    if (this.tasks[name]) {
        // fail silently for now since plugins will attempt to
        // reregister tasks on reconnect
    } else {
        let that = this;
        this.tasks[name] = {
            interval: minutes,
            action: setInterval(function() {
                process.nextTick(function() {
                    that.info(`Running Scheduled Task => ${name}`);
                    callback();
                });
            }, timer),
        };
        callback();
    }
  }

  /**
   * Uses momentjs to create a quick date and time string
   * @return {string} a date and time strng
   */
  getMomentStamp() {
    return moment().format('YYYY-MM-DD hh:mm:ss');
  }

  /**
   * logs informational messages to the console
   * @param {string} msg the message to log
   */
  info(msg) {
    console.info(chalk.cyan(`[${this.getMomentStamp()}]: `) + `${msg}`);
  }

  /**
   * logs a message to the console
   * @param {string} msg the message to log
   */
  log(msg) {
    console.log(chalk.cyan(`[${this.getMomentStamp()}]: ${msg}`));
  }

  /**
   * logs a warning message to the console
   * @param {string} msg  the warning message to log
   */
  warn(msg) {
    if (msg) {
      console.warn(chalk.yellow(`[${this.getMomentStamp()}]: ${msg}`));
    }
  }

  /**
   * logs an error message to the console and closes the script
   * @param {string} msg the error message to log
   */
  error(msg) {
    console.error(chalk.red(`[${this.getMomentStamp()}]: ${msg}`));
  }
};

module.exports = AurigaClient;
