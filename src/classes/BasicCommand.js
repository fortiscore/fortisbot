/**
 * class representing a basic command
 * @alias _.BasicCommand
 */
class BasicCommand {
  /** creates the command
   * @param {object} auriga the auriga client reference
   * @param {string} name the command name decided by auriga
   * @param {object} settings the command settings loaded from json
  */
  constructor(auriga, name, settings) {
    this._auriga     = auriga;
    this._name       = name;
    this._settings   = settings;
    this._registered = false;
    this._type       = 'command';
  }

  /**
   * code to run on load
   */
  init() {
    this.register();
  }

  /**
   * runs the command code when called
   * @param {string} message the message received from the service
   * @param {object} bundle the bundled meta data provided by the service
   * @param {string} service the service that called the action
   * @return {promise} resolves with response when finished, rejects on errors
   */
  action(message, bundle={}, service) {
    return Promise.resolve();
  }

  /**
   * registers the command
   */
  register() {
    if (!this._registered) {
      this._registered = true;
      this._auriga.registerCommand(this._name, this);
    }
  }

  /**
   * logs informational messages to the console
   * @param {string} msg the message to log
   */
  info(msg) {
    this._auriga.info(`[Command:${this._name}]: ${msg}`);
  }

  /**
   * logs a message to the console
   * @param {string} msg the message to log
   */
  log(msg) {
    this._auriga.log(`[Command:${this._name}]: ${msg}`);
  }

  /**
   * logs a warning message to the console
   * @param {string} msg  the warning message to log
   */
  warn(msg) {
    this._auriga.warn(`[Command:${this._name}]: ${msg}`);
  }

  /**
   * logs an error message to the console and closes the script
   * @param {string} msg the error message to log
   */
  error(msg) {
    this._auriga.error(`[Command:${this._name}]: ${msg}`);
  }
};

module.exports = BasicCommand;
