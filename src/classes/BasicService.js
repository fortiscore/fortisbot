/**
 * class representing a basic service
 * @alias _.BasicService
 */
class BasicService {
  /** creates the service
   * @param {object} auriga the auriga client reference
   * @param {string} name the service name decided by auriga
   * @param {object} settings the service settings loaded from json
  */
  constructor(auriga, name, settings) {
    this._auriga     = auriga;
    this._name       = name;
    this._settings   = settings;
    this._registered = false;
    this._type       = 'service';
  }

  /**
   * authenticates a user against a keychain
   * @param {object} keychain an array of keychain entries to check against
   * @param {object} bundle an object of meta data initially sent by the service
   */
  authenticate(keychain, bundle) {
    throw new Error(`${this._name} has no authenticate() method defined.`);
  }

  /**
   * code to run on load
   */
  init() {
    throw new Error(`${this._name} has no init() method defined.`);
  }

  /**
   * handle a response from a command
   * @param {object} bundle the original message bundle with command response
   *                        added
   */
  respond(bundle) {
    throw new Error(`${this._name} has no respond() method defined.`);
  }

  /**
   * dispatches new messages to the auriga client
   * @param {string} message the message received from the service
   * @param {object} bundle any bundled information the service will need later
   */
  dispatchMessage(message, bundle) {
    this._auriga.dispatchedMessage(this._name, message, bundle);
  }

  /**
   * registers the service
   */
  register() {
    if (!this._registered) {
      this._registered = true;
      this._auriga.registerService(this._name, this);
    }
  }

  /**
   * logs informational messages to the console
   * @param {string} msg the message to log
   */
  info(msg) {
    this._auriga.info(`[Service:${this._name}]: ${msg}`);
  }

  /**
   * logs a message to the console
   * @param {string} msg the message to log
   */
  log(msg) {
    this._auriga.log(`[Service:${this._name}]: ${msg}`);
  }

  /**
   * logs a warning message to the console
   * @param {string} msg  the warning message to log
   */
  warn(msg) {
    this._auriga.warn(`[Service:${this._name}]: ${msg}`);
  }

  /**
   * logs an error message to the console and closes the script
   * @param {string} msg the error message to log
   */
  error(msg) {
    this._auriga.error(`[Service:${this._name}]: ${msg}`);
  }
};

module.exports = BasicService;
